# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import pandas as pd
import numpy as np
import scipy
from sklearn import svm
import sklearn.metrics as metrics
from operator import itemgetter
from sklearn.externals import joblib
import time

import settings
import splice_site_utils


class SpliceSitePredictionModel:
    def getModelFilename(self, siteType):
        modelFilename = settings.MODEL_FILE_NAMES[siteType.lower()]
        return modelFilename

    def loadModel(self, modelFilename):
        print('####\nLoading previously trained model: ' + modelFilename)
        return joblib.load(modelFilename)

    def loadCandidateFeaturesFile(self, filename):
        # load test data and get list of sequences
        print('####\nLoading combined feature data for prediction: ' + filename)
        df = pd.read_csv(filename)
        print('Original df.shape: ' + str(df.shape))

        # remove rows where the sequence has N because that generates NAs
        df_clean = df[df.Sequence.str.split('-').str.get(0).str.contains('N') == False]
        print('df_clean.shape: ' + str(df_clean.shape))

        return df_clean

    def getSequencesFromFeaturesData(self, df):
        sequences = [col for col in df.columns if col == 'Sequence']
        sequence_df = df[sequences]
        sequence_df = sequence_df.values
        sequence_list = sequence_df.flatten().tolist()
        print('len(sequence_list): ' + str(len(sequence_list)))
        return sequence_list

    def getFeatures(self, df):
        # load features that match model
        feature_columns = [col for col in df.columns if col != 'Sequence']
        features = df[feature_columns].values  # remove column names and non-feature columns and output as np array

        print('####\nTesting feature array for NaN or inf...')
        print(np.isnan(np.sum(features)))

        return features

    def getPredictedLabels(self, clf, features):
        # perform prediction
        print('####\nPerforming prediction on test data using model...')
        return clf.predict(features)

    def getPositiveEstimates(self, clf, features):
        # retrieve probability estimates to compute auROC and auPRC
        return clf.predict_proba(features)[:, 1]  # [1] column of array is class=1

    def writePredictionsToFile(self, candidatesFeaturesFilename, modelFilename, sequences, predictedLabels, positiveEstimates, outputFilename):
        outfile = open(outputFilename, 'wt')
        outfile.write('>SVM model: ' + modelFilename + '\n')
        outfile.write('>Test data: ' + candidatesFeaturesFilename + '\n')

        outfile.write('>Sequence_ID\tPrediction\tProbability_Estimate\n')
        for seq, label, est in zip(sequences, predictedLabels, np.round(positiveEstimates, 6)):
            if label == 0.0:
                outfile.write(seq + '\tfalse\t' + str(est) + '\n')
            elif label == 1.0:
                outfile.write(seq + '\ttrue\t' + str(est) + '\n')

        outfile.close()

    def performPredictionsAndWriteOutputToFile(self, siteType, candidatesFeaturesFilename, outputFilename = None):
        modelFilename = self.getModelFilename(siteType)
        clf = self.loadModel(modelFilename)
        df = self.loadCandidateFeaturesFile(candidatesFeaturesFilename)
        sequence_list = self.getSequencesFromFeaturesData(df)
        features = self.getFeatures(df)
        predictedLabels = []
        positiveEstimates = []

        if len(sequence_list) > 0:
            predictedLabels = self.getPredictedLabels(clf, features)
            positiveEstimates = self.getPositiveEstimates(clf, features)

        if outputFilename is None:
            timestamp = splice_site_utils.getFileTimeStamp()
            candidatesBaseFilename = os.path.basename(candidatesFeaturesFilename)
            outputFilename = 'prediction_' + timestamp + '_on_' + candidatesBaseFilename[:-4] + '.txt'

        self.writePredictionsToFile(candidatesFeaturesFilename, modelFilename, sequence_list, predictedLabels, positiveEstimates, outputFilename)

        return outputFilename

def printUsage():
    print("splice_site_prediction_model.py siteType candidatesFeatures.csv")

def main():
    # command line arguments #
    if len(sys.argv) == 3:
        siteType = sys.argv[1]  # specify "donors" or "acceptors"
        assert siteType == 'donors' or siteType == 'acceptors', 'Please specify splice site type as donors or acceptors in first argument.'
        candidatesFeaturesFilename = sys.argv[2]  # combined CSV of all features
        outputFilename = SpliceSitePredictionModel().performPredictionsAndWriteOutputToFile(siteType, candidatesFeaturesFilename)

        print('Results written to ' + outputFilename)
    else:
        printUsage()

if __name__ == '__main__':
    main()
