# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import collections
import os
import sys

import splice_site_utils


class PositionFeatureExtractor:
    def __init__(self, siteType, windowSize):
        self.siteType = siteType
        self.windowSize = windowSize

    ### CREATE DICT OF INPUT SEQUENCES THAT NEED FEATURES EXTRACTED ###
    """
    Purpose: To trim sequences based on origin file and site type (i.e. donor, acceptor).
    Arguments: Sequence to be trimmed, splice site type, window size of interest, filename that contains "true" or "false".
    Returns: Trimmed splice sequence.
    """
    def trimSeq(self, sequence):
        return splice_site_utils.trimSeq(sequence, self.siteType, self.windowSize)
    # def trimSeq(self, sequence):
    #     if self.windowSize == 'full':
    #         trimmedSeq = sequence.strip()
    #     else:
    #         tokens = self.windowSize.split('/')
    #         left, right = int(tokens[0]), int(tokens[1])
    #         if self.siteType == 'donors':
    #             if left == 1:
    #                 x = 6
    #             elif left == 3:
    #                 x = 4
    #             elif left == 5:
    #                 x = 2
    #             if right == 3:
    #                 y = 12
    #             elif right == 5:
    #                 y = 14
    #             elif right == 7:
    #                 y, z = 15, 'N' # add artificial base to fill window
    #             elif right == 9:
    #                 y, z = 15, 'NNN'
    #         elif self.siteType == 'acceptors':
    #             x = max
    #             if left == 20:
    #                 x = 48
    #             elif left == 22:
    #                 x = 46
    #             elif left == 24:
    #                 x = 48
    #             if right == 1:
    #                 y = 71
    #             elif right == 3:
    #                 y = 73
    #             elif right == 5:
    #                 y = 75
    #
    #         try:
    #             trimmedSeq = sequence[x:y] + z
    #         except NameError: # if z is not defined do this below
    #             trimmedSeq = sequence[x:y]
    #     return trimmedSeq

    def readTrimmedSequencesFromCandidateFile(self, candidateFilename):
        candidateFile = open(candidateFilename)
        inputSeqs = collections.OrderedDict() # keep key as seqFull and value as trimmedSeq
        lineNum = 1 # counter for duplicate sequences
        seqLength = 0

        for line in candidateFile:
            key, seqFull = line.split('\t')[0], line.split('\t')[1].strip()
            trimmedSeq = self.trimSeq(seqFull)
            seqLength = len(trimmedSeq) # save for creating feature column names
            seqFull += '-' + '-'.join(key.split(' ')) + '-' + str(lineNum) # add line number to make unique after trimming has occurred
            inputSeqs[seqFull] = trimmedSeq
            lineNum += 1

        candidateFile.close()

        return inputSeqs, seqLength

    def trimSequences(self, candidates):
        inputSeqs = collections.OrderedDict() # keep key as seqFull and value as trimmedSeq
        lineNum = 1  # counter for duplicate sequences
        seqLength = 0

        for candidate in candidates:
            seqFull = candidate.seq
            trimmedSeq = self.trimSeq(seqFull)
            seqLength = len(trimmedSeq) # save for creating feature column names
            key = seqFull + '-' + candidate.seq_id + '-' + str(lineNum) # add line number to make unique after trimming has occurred
            inputSeqs[key] = trimmedSeq
            lineNum += 1

        return inputSeqs, seqLength

    ### CREATE LIST OF FEATURE COLUMN NAMES ###
    def getFeatureColumnHeader(self, seqLength):
        bases = ['A', 'T', 'C', 'G']
        headerList = [] # create beauteous list of column headers

        for n in range(1, seqLength + 1): # naming for humans
            for base in bases:
                headerList.append('f' + str(n) + '(' + base + ')')

        return headerList

    def writePosFeatures(self, inputSequenceDict, outFile, headerList): # a function to write the features
        outFile.write('Sequence\t')
        outFile.write('\t'.join(headerList) + '\n')
        for inputSeqFull, inputTrimmedSeq in inputSequenceDict.items():
            outFile.write(inputSeqFull + '\t')
            seqBases = list(inputTrimmedSeq)
            for header in headerList: # iterate in same order as header
                pos, base = int(header.split('(')[0][1:])-1, header.split('(')[1][:-1] # retrieves positions that may be >1 digit for pos
                if seqBases[pos] == 'N': # check for artificial bases to create NA feature
                    outFile.write('NA')
                elif seqBases[pos] == base:
                    outFile.write('1')
                else:
                    outFile.write('0')
                outFile.write('\t')
            outFile.write('\n')
        outFile.close()

    def extractAndWritePositionFeaturesFile(self, candidates_file_name, candidates, output_filename=None):
        # inputSeqs, seqLength = self.readTrimmedSequencesFromCandidateFile(candidateFileName, candidates_sequences)
        inputSeqs, seqLength = self.trimSequences(candidates)
        columnHeaders = self.getFeatureColumnHeader(seqLength)

        if output_filename is None:
            baseFilename = os.path.basename(candidates_file_name)
            formatedWindowSize = splice_site_utils.formatWindowSizeString(self.windowSize)
            output_filename = baseFilename[:-4] + '_' + formatedWindowSize + '_pos_simple.txt'

        outputFile = open(output_filename, 'wt')

        self.writePosFeatures(inputSeqs, outputFile, columnHeaders)

        return output_filename

def main():
    ### COMMAND LINE ARGUMENTS ###
    siteType = sys.argv[1] # specify "donors" or "acceptors"
    assert siteType == 'donors' or siteType == 'acceptors', 'Please specify splice site type as donors or acceptors in first argument.'
    input_filename = sys.argv[2] # cleaned true positives input
    windowSize = input('Enter the window size of interest, e.g. +3,-5 is \'3/5\' or \'full\' for full window: ')

    # inputFile = open(input_filename)
    # outFile = open(input_filename[:-4] + '_-' + windowSize.split('/')[0] + '+' + windowSize.split('/')[1] + '_pos_simple.txt', 'wt') # name file with window size
    candidates = splice_site_utils.getCandidates(input_filename)
    PositionFeatureExtractor(siteType, windowSize).extractAndWritePositionFeaturesFile(input_filename, candidates)

if __name__ == '__main__':
    main()