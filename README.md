# CryptSplice #
Software for selecting high confidence candidate splice sequences utilizing a SVM machine learning model, variant distance from canonical splice sites, and changes in splice potential (Δvariant and Δcanon)


## Splice Sequence Categories ##
* Weakens the canonical splice site
* Activates a cryptic splice site which could outcompete the canonical
* Activates a cryptic splice site in the deep intron

## Disclaimer ##
Please beware predictions may not be accurate. Not for use for diagnosis.

## Workflow ##
![Workflow.png](https://bitbucket.org/repo/7d7zey/images/585831320-Workflow.png)

## Tool Documentation ##

Go to our [Wiki](https://bitbucket.org/jhucidr/cryptsplice/wiki/Home) page for tool documentation:

1. [Setup Guide](https://bitbucket.org/jhucidr/cryptsplice/wiki/Setup%20Guide): A step-by-step guide to downloading SpliceVar, setting up the environment, and running your variants of interest.

2. [Input File Format](https://bitbucket.org/jhucidr/cryptsplice/wiki/Input%20File%20Format): Format required for input variants

3. [Program Settings](https://bitbucket.org/jhucidr/cryptsplice/wiki/Program%20Settings): The settings.py file contains multiple workflow options that can be adjusted.

## Contact ##

For any questions, please submit an issue at the following [link](https://bitbucket.org/jhucidr/cryptsplice/issues?status=new&status=open)