# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sqlite3
import settings
import pickle

import splice_site_utils

cftr_chromosome = '7'
cftr_accession = 'NM_000492'
cftr_exons = [[117120150, 117120201, 'exon1'], [117144307, 117144417, 'exon2'], [117149088, 117149196, 'exon3'],
              [117170953, 117171168, 'exon4'], [117174330, 117174419, 'exon5'], [117175302, 117175465, 'exon6'],
              [117176602, 117176727, 'exon7'], [117180154, 117180400, 'exon8'], [117182070, 117182162, 'exon9'],
              [117188695, 117188877, 'exon10'], [117199518, 117199709, 'exon11'], [117227793, 117227887, 'exon12'],
              [117230407, 117230493, 'exon13'], [117231988, 117232711, 'exon14'], [117234984, 117235112, 'exon15'],
              [117242880, 117242917, 'exon16'], [117243586, 117243836, 'exon17'], [117246728, 117246807, 'exon18'],
              [117250573, 117250723, 'exon19'], [117251635, 117251862, 'exon20'], [117254667, 117254767, 'exon21'],
              [117267576, 117267824, 'exon22'], [117282492, 117282647, 'exon23'], [117292896, 117292985, 'exon24'],
              [117304742, 117304914, 'exon25'], [117305513, 117305618, 'exon26'], [117306962, 117307164, 'exon27']]

def createExonsTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Exon (accession TEXT, start INT, end INT)')

    print('\tWriting content to DB...')
    for exonCoords in cftr_exons:
        rowTuple = (cftr_accession, exonCoords[0], exonCoords[1])
        cursor.execute('INSERT INTO Exon VALUES (?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_index ON Exon (accession)')


def extractWildTypePredictionValuesAsRowTuple(value):
    seqID = value[1]
    prob = value[2]

    seqIDTokens = seqID.split('-')
    seq = seqIDTokens[0]
    position = seqIDTokens[1]

    accession_location = splice_site_utils.buildAccessionLocationKey(cftr_accession, cftr_chromosome, position)

    return (accession_location, seq, prob, seqID)


def createWildTypeDonorPredicitionTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Wild_Type_Donor_Prediction '
                   + '(accession_location TEXT, sequence TEXT, probability REAL, sequence_id TEXT)')

    with open('data/cftr_wt_donor_predictions.pkl', 'rb') as f:
        data = pickle.load(f)
        print('\tLoaded WT Donor Predictions file...')
        print('\tWriting file content to DB...')
        for coordinate, value in data.items():
            rowTuple = extractWildTypePredictionValuesAsRowTuple(value)
            cursor.execute('INSERT INTO Wild_Type_Donor_Prediction VALUES (?,?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_location_donor_index '
                   + 'ON Wild_Type_Donor_Prediction (accession_location)')


def createWildTypeAcceptorPredicitionTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Wild_Type_Acceptor_Prediction '
                   + '(accession_location TEXT, sequence TEXT, probability REAL, sequence_id TEXT)')

    with open('data/cftr_wt_acceptor_predictions.pkl', 'rb') as f:
        data = pickle.load(f)
        print('\tLoaded WT Acceptor Predictions file...')
        print('\tWriting file content to DB...')
        for coordinate, value in data.items():
            rowTuple = extractWildTypePredictionValuesAsRowTuple(value)
            cursor.execute('INSERT INTO Wild_Type_Acceptor_Prediction VALUES (?,?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_location_acceptor_index '
                   + 'ON Wild_Type_Acceptor_Prediction (accession_location)')


def main():
    connection = sqlite3.connect('data/cftr_db.sqllite3')
    cursor = connection.cursor()

    print('Creating Exon table...')
    createExonsTable(cursor)
    connection.commit()
    print('\tFinished')

    print('Creating Wild Type Donor Prediction table...')
    createWildTypeDonorPredicitionTable(cursor)
    connection.commit()
    print('\tFinished')

    print('Creating Wild Type Acceptor Prediction table...')
    createWildTypeAcceptorPredicitionTable(cursor)
    connection.commit()
    print('\tFinished')

    connection.close()


main()
