# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import pandas as pd
import numpy as np
import scipy
import pickle as pkl
from sklearn import svm
from sklearn import cross_validation
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV, RandomizedSearchCV
import sklearn.metrics as metrics
from operator import itemgetter
from sklearn.externals import joblib
import time


def main():
    # csv with all features and all samples from hs3d+nn269 with true and false class column
    df = pd.read_csv(open(sys.argv[1]))
    kernel = sys.argv[2]
    assert kernel == "linear" or kernel == "RBF", "Must specify linear or RBF as kernel (second argument)."
    timestamp = time.strftime("%Y-%m-%d")
    out_file = open('_'.join(sys.argv[1].split('_')[:-1])
                    + '_SVM_' + kernel + '_nofeatselect_10foldparamoptscores_prob=True_'
                    + timestamp + '.txt',
                    'wt')

    model_filename = 'model_SVM_' + kernel + '_nofeatselect_10foldparamopt_prob=True_' + sys.argv[1].split('/')[-1][
                                                                                         :-4] + '_' + timestamp + '.pkl'  # save in same folder
    open(model_filename, 'a').close()  # verify path is acceptable instead of after hours of training

    print('####\nPreparing dataframe of features from ' + sys.argv[1] + '...')
    feature_columns = [col for col in df.columns if (col != 'Sequence' and col != 'class')]
    features = df[feature_columns].values  # remove column names and non-feature columns
    labels = df['class'].values

    print('Dataframe shape: ' + str(df.shape))
    print('Number of samples with labels: ' + str(len(labels)))
    print('Number of samples with features: ' + str(len(features)))

    # 10-fold cross validation
    print('####\nPerforming 10-fold cross validation to find parameters that yield the best model...')
    RANDOMIZEDSEARCH = True  # whether to do randomized grid search or not
    param_dist = getSVMparametersToOptimize(kernel, RANDOMIZEDSEARCH)
    if RANDOMIZEDSEARCH:
        # probability=True to calculate probability estimates for computing auPRC+auROC during prediction -- will make clf.fit() slow!
        clf = RandomizedSearchCV(svm.SVC(probability=True), param_distributions=param_dist, n_iter=100, cv=10,
                                 scoring="recall")
        clf.fit(features, labels)
    else:
        clf = GridSearchCV(svm.SVC(C=1, probability=True), param_distributions=param_dist, cv=10, scoring="recall")
        clf.fit(features, labels)

    # retrieve probability estimates for positive class to compute auROC and auPRC
    positive_estimates = clf.predict_proba(features)[:, 1]  # [1] column of array is class=1
    auROC = metrics.roc_auc_score(labels, positive_estimates)
    auPRC = metrics.average_precision_score(labels, positive_estimates)

    print('auROC: ' + str(round(auROC, 6)))
    print('auPRC: ' + str(round(auPRC, 6)))

    # report best models and save top model
    print('####\nReporting the best model and performance scores...')
    report(clf.grid_scores_, out_file, auROC, auPRC)

    print('####\nSaving the best model...')
    joblib.dump(clf.best_estimator_, model_filename)

    out_file.close()


# create dictionary of parameters for optimization
def getSVMparametersToOptimize(kernel, randomsearch):
    if randomsearch:
        if kernel == "RBF":
            param_dist = {'C': scipy.stats.expon(scale=100), 'gamma': scipy.stats.expon(scale=.1),
                          'kernel': ['rbf']}  # , 'class_weight':['auto', None]}
        else:
            param_dist = {'C': scipy.stats.expon(scale=100), 'kernel': ['linear']}
    else:
        if kernel == "RBF":
            param_dist = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4], 'C': [1, 10, 100, 1000]}, ]
        else:
            param_dist = [{'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
    return param_dist


# writes a report of grid-based model optimization outfile
def report(grid_scores, out_file, auROC, auPRC, n_top=3):
    out_file.write('auROC: ' + str(round(auROC, 6)) + '\n')
    out_file.write('auPRC: ' + str(round(auPRC, 6)) + '\n\n')

    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        out_file.write("Model with rank: {0}\n".format(i + 1))
        out_file.write("Mean validation score: {0:.3f} (std: {1:.3f})\n".format(score.mean_validation_score,
                                                                                np.std(score.cv_validation_scores)))
        out_file.write("Parameters: {0}\n\n".format(score.parameters))


main()
