# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import sys

import time

import pickle

import candidates_file_parser
import settings
import site_mutation_check
import splice_site_utils
import variant_validator
from combine_test_feature_files import CandidateFeatureCombiner
from create_candidate_splice_sites_fromVCF import CandidateSpliceSiteExtractor
from extract_apr_test_simple import APRFeatureExtractor
from extract_component_test import ComponentFeatureExtractor
from extract_pos_test_simple import PositionFeatureExtractor
from prioritize_predictions import PrioritizePredicitions
from ref_seq_database import RefSeqDatabase
from splice_site_prediction_model import SpliceSitePredictionModel
from variant_validator import VariantValidator


def extractCandidateSpliceSites(vcfFilename):
    donorCandidatesFilename, acceptorCandidatesFilename = CandidateSpliceSiteExtractor(
        vcfFilename).generateCandidateSpliceSites()
    return donorCandidatesFilename, acceptorCandidatesFilename


def extractCandidateFeaturesFromCandidatesFile(site_type, candidates_filename, candidates, component_window_size, pos_window_size,
                                               apr_window_size):
    posFeaturesFilename = PositionFeatureExtractor(site_type, pos_window_size).extractAndWritePositionFeaturesFile(
        candidates_filename, candidates)
    aprFeaturesFilename = APRFeatureExtractor(site_type, apr_window_size)\
        .extractAndWriteAPRFeaturesFile(candidates_filename, candidates)
    componentFeaturesFilename = ComponentFeatureExtractor(site_type, component_window_size)\
        .writeComponentTestFile(candidates_filename, candidates)

    return CandidateFeatureCombiner(site_type).writeCombinedFeatureFile(componentFeaturesFilename, posFeaturesFilename,
                                                                        aprFeaturesFilename)


def validate_donor_candidates(candidates, ref_seq_db):
    validator = VariantValidator(candidates, ref_seq_db)
    validator.validate_donor_candidates()
    print("Excluding donor candidates:")
    validator.pretty_print_invalid_candidates()
    return validator.valid_candidates


def validate_acceptor_candidates(candidates, ref_seq_db):
    validator = VariantValidator(candidates, ref_seq_db)
    validator.validate_acceptor_candidates()
    print("Excluding acceptor candidates:")
    validator.pretty_print_invalid_candidates()
    return validator.valid_candidates


def get_candidates(donor_candidates_filename, acceptor_candidates_filename):
    donor_candidates = candidates_file_parser.get_candidates(donor_candidates_filename)
    acceptor_candidates = candidates_file_parser.get_candidates(acceptor_candidates_filename)

    if settings.VALIDATE_USER_SUPPLIED_VARIANTS:
        print('Validating Candidates')
        ref_seq_db = RefSeqDatabase()
        ref_seq_db.connect()
        donor_candidates = validate_donor_candidates(donor_candidates, ref_seq_db)
        acceptor_candidates = validate_acceptor_candidates(acceptor_candidates, ref_seq_db)
        ref_seq_db.close()

    return donor_candidates, acceptor_candidates


def extractCandidateFeatures(donor_candidates_filename, acceptor_candidates_filename):
    donor_candidates, acceptor_candidates = get_candidates(donor_candidates_filename, acceptor_candidates_filename)
    num_donor_candidates = len(donor_candidates)
    num_acceptor_candidates = len(acceptor_candidates)

    print("Number of donor candidates to process: " + str(num_donor_candidates))
    print("Number of acceptor candidates to process: " + str(num_acceptor_candidates))

    assert num_donor_candidates > 0, "No donor candidates to process"
    assert num_acceptor_candidates> 0, "No acceptor candidates to process"

    donorFeaturesFilename = extractCandidateFeaturesFromCandidatesFile('donors', donor_candidates_filename,
                                                                       donor_candidates,
                                                                       settings.DONORS_COMPONENT_FEATURE_EXTRACTION_MULTISCALE_VALUE,
                                                                       settings.DONORS_POS_FEATURE_EXTRACTION_WINDOW_SIZE,
                                                                       settings.DONORS_APR_FEATURE_EXTRACTION_WINDOW_SIZE,
                                                                       )
    acceptorFeaturesFilename = extractCandidateFeaturesFromCandidatesFile('acceptors', acceptor_candidates_filename,
                                                                          acceptor_candidates,
                                                                          settings.ACCEPTOR_COMPONENT_FEATURE_EXTRACTION_MULTISCALE_VALUE,
                                                                          settings.ACCEPTOR_POS_FEATURE_EXTRACTION_WINDOW_SIZE,
                                                                          settings.ACCEPTOR_APR_FEATURE_EXTRACTION_WINDOW_SIZE,
                                                                          )
    return donorFeaturesFilename, acceptorFeaturesFilename


def performPredictions(donorFeaturesFilename, acceptorFeaturesFilename):
    spliceSitePredictionModel = SpliceSitePredictionModel()
    donorPredictionsFilename = spliceSitePredictionModel.performPredictionsAndWriteOutputToFile('donors',
                                                                                                donorFeaturesFilename)
    acceptorPredictionsFilename = spliceSitePredictionModel.performPredictionsAndWriteOutputToFile('acceptors',
                                                                                                   acceptorFeaturesFilename)
    return donorPredictionsFilename, acceptorPredictionsFilename


def priortizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                         acceptorPredictionsFilename, acceptorCandidatesFilename):
    prioritizedPredictionsFilename = PrioritizePredicitions(settings.DELTA_VARIANT_THRESHOLD,
                                                            settings.DELTA_CANONICAL_THRESHOLD) \
        .prioritizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                               acceptorPredictionsFilename, acceptorCandidatesFilename)
    return prioritizedPredictionsFilename


def printUsageMessage():
    print("Usage:\tmain.py -extract_candidates \n\t\tvcf_file\n" +
          "\tmain.py -extract_features\n\t\tdonor_candidates_file \n\t\tacceptor_candidates_file\n" +
          "\tmain.py -make_predictions\n\t\tdonor_candidates_file \n\t\tdonor_candidates_combined_features_file\n\t\tacceptor_candidates_file \n\t\tacceptor_candidates_combined_features_file\n" +
          "\tmain.py -prioritize_predictions\n\t\tdonorPredictionsFilename\n\t\tdonorCandidatesFilename\n\t\tacceptorPredictionsFilename\n\t\tacceptorCandidatesFilename\n")


def startWithCandidateExtraction(vcfFilename):
    donorCandidatesFilename, acceptorCandidatesFilename = extractCandidateSpliceSites(vcfFilename)
    donorFeaturesFilename, acceptorFeaturesFilename = extractCandidateFeatures(donorCandidatesFilename,
                                                                               acceptorCandidatesFilename)
    donorPredictionsFilename, acceptorPredictionsFilename = performPredictions(donorFeaturesFilename,
                                                                               acceptorFeaturesFilename)
    priorityPredictionsFilename = priortizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                                                       acceptorPredictionsFilename, acceptorCandidatesFilename)
    return priorityPredictionsFilename


def startWithExtractFeatures(donorCandidatesFilename, acceptorCandidatesFilename):
    donorFeaturesFilename, acceptorFeaturesFilename = extractCandidateFeatures(donorCandidatesFilename,
                                                                               acceptorCandidatesFilename)
    donorPredictionsFilename, acceptorPredictionsFilename = performPredictions(donorFeaturesFilename,
                                                                               acceptorFeaturesFilename)
    priorityPredictionsFilename = priortizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                                                       acceptorPredictionsFilename, acceptorCandidatesFilename)
    return priorityPredictionsFilename


def startWithMakePredicitions(donorCandidatesFilename, donorFeaturesFilename,
                              acceptorCandidatesFilename, acceptorFeaturesFilename):
    donorPredictionsFilename, acceptorPredictionsFilename = performPredictions(donorFeaturesFilename,
                                                                               acceptorFeaturesFilename)
    priorityPredictionsFilename = priortizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                                                       acceptorPredictionsFilename, acceptorCandidatesFilename)
    return priorityPredictionsFilename


def startWithPrioritizePredicitions(donorPredictionsFilename, donorCandidatesFilename,
                                    acceptorPredictionsFilename, acceptorCandidatesFilename):
    priorityPredictionsFilename = priortizePredictions(donorPredictionsFilename, donorCandidatesFilename,
                                                       acceptorPredictionsFilename, acceptorCandidatesFilename)
    return priorityPredictionsFilename


def printFinishedMessage(outputFilename):
    print('Results written to: ' + outputFilename)


def main():
    numArgs = len(sys.argv)
    if numArgs < 2:
        printUsageMessage()
    else:
        startWithFlag = sys.argv[1]
        print(startWithFlag)
        if startWithFlag == '-extract_candidates' and numArgs == 3:
            vcfFilename = sys.argv[2]
            outputFilename = startWithCandidateExtraction(vcfFilename)
            printFinishedMessage(outputFilename)
        elif startWithFlag == '-extract_features' and numArgs == 4:
            donorCandidatesFilename = sys.argv[2]
            acceptorCandidatesFilename = sys.argv[3]
            outputFilename = startWithExtractFeatures(donorCandidatesFilename, acceptorCandidatesFilename)
            printFinishedMessage(outputFilename)
        elif startWithFlag == '-make_predictions' and numArgs == 6:
            donorCandidatesFilename = sys.argv[2]
            donorFeaturesFilename = sys.argv[3]
            acceptorCandidatesFilename = sys.argv[4]
            acceptorFeaturesFilename = sys.argv[5]
            outputFilename = startWithMakePredicitions(donorCandidatesFilename, donorFeaturesFilename,
                                                       acceptorCandidatesFilename, acceptorFeaturesFilename)
            printFinishedMessage(outputFilename)
        elif startWithFlag == '-prioritize_predictions' and numArgs == 6:
            donorPredictionsFilename = sys.argv[2]
            donorCandidatesFilename = sys.argv[3]
            acceptorPredictionsFilename = sys.argv[4]
            acceptorCandidatesFilename = sys.argv[5]
            outputFilename = startWithPrioritizePredicitions(donorPredictionsFilename, donorCandidatesFilename,
                                                             acceptorPredictionsFilename, acceptorCandidatesFilename)
            printFinishedMessage(outputFilename)
        else:
            printUsageMessage()


main()
