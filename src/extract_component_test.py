# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
from __future__ import division

import os
import sys
import itertools
import re

import collections

import splice_site_utils


class ComponentFeatureExtractor:
    def __init__(self, siteType, multiscaleComponent):
        self.siteType = siteType
        self.multiscaleComponent = multiscaleComponent

    def getInputSequences(self, candidates):
        inputSeqs = []
        lineNum = 1 # counter for duplicate sequences

        for candidate in candidates:
            seqFull = candidate.seq + '-' + candidate.seq_id + '-' + str(lineNum)  # add line number to make unique after trimming has occurred
            inputSeqs.append(seqFull)
            lineNum += 1

        return inputSeqs

    # function to calculate probabilities
    def getProb(self, freq, length, k):
        prob = freq / (length - k + 1)
        return prob

    # function to trim input sequences by type (ie donor vs acceptor)
    def trimSeqs(self, inputSeq):
        if self.siteType.lower() == 'donors':  # leave out consensus dinucleotide
            seqLeft = inputSeq[:-8]
            seqRight = inputSeq[9:]
        elif self.siteType.lower() == 'acceptors':  # leave out consensus dinucleotide
            seqLeft = inputSeq[:-22]
            seqRight = inputSeq[70:]
        return seqLeft, seqRight

    ### GENERATE ALL POSSIBLE SUBSTRING COMBINATIONS ###
    def generateComponentSubstrings(self):
        print('####\nGenerating all possible component substrings...')

        if self.multiscaleComponent.lower() == 'full':
            a, b = 1, 5  # make all possible k
        else:
            tokens = self.multiscaleComponent.split('-')
            a, b = int(tokens[0]), int(tokens[1])

        all_combos = set()  # will not accept duplicates

        for k in range(a, b + 1):
            for combo_list in itertools.permutations('ATCG', k):  # all possible unique combos
                combo_string = ''.join(combo_list)
                all_combos.add(combo_string)
            for combo_list in itertools.product('ATCG', repeat=k):  # all possible combos with repeated nucleotides
                combo_string = ''.join(combo_list)
                all_combos.add(combo_string)

        return list(sorted(all_combos))

    def writeComponentTestFile(self, candidates_filename, candidates, output_filename = None):
        inputSeqs = self.getInputSequences(candidates)
        allCombos = self.generateComponentSubstrings()

        if output_filename is None:
            baseFilename = os.path.basename(candidates_filename)
            output_filename = baseFilename[:-4] + '_component_k=' + self.multiscaleComponent + '.txt'

        ### PREPARE OUTPUT FILE ###
        # out = open(input_filename[:-4] + '_component_k=' + msc + '.txt', 'wt') # initialize output file in current directory
        # outputFile = open(inputFilename[:-4] + '_component_k=' + msc + '.txt', 'wt')

        outputFile = open(output_filename, 'wt')
        outputFile.write('Sequence\t')  # write header
        for combo in allCombos:
            outputFile.write('P(' + combo + ')left\tP(' + combo + ')right\t')
        outputFile.write('\n')

        ### EXTRACT COMPONENT FEATURES ###
        print('####\nExtracting component features...')
        for inputSeq in inputSeqs:
            outputFile.write(inputSeq + '\t')
            seqLeft, seqRight = self.trimSeqs(inputSeq.split('-')[0])
            for combo in allCombos:
                # use re.findall with lookahead assertion for overlapping matches
                freqLeft, freqRight = len(re.findall(r'(?=(' + combo + '))', seqLeft)), len(
                    re.findall(r'(?=(' + combo + '))', seqRight))
                if freqLeft > 0:
                    probLeft = self.getProb(freqLeft, len(seqLeft), len(combo))
                else:
                    probLeft = 0
                if freqRight > 0:
                    probRight = self.getProb(freqRight, len(seqRight), len(combo))
                else:
                    probRight = 0
                outputFile.write(str(probLeft) + '\t' + str(probRight) + '\t')
            outputFile.write('\n')

        outputFile.close()

        return output_filename

def main():
    ### COMMAND LINE ARGUMENTS ###
    site_type = sys.argv[1]  # specify "donors" or "acceptors"
    assert site_type == 'donors' or site_type == 'acceptors', 'Please specify splice site type as donors or acceptors in first argument.'
    candidates_filename = sys.argv[2]  # cleaned input
    msc = input('Enter the multiscale component of interest, e.g. k=1~4 is \'1-4\' or \'full\' for all possible k: ')
    candidates = splice_site_utils.getCandidates(candidates_filename)

    ### CREATE SET OF INPUT SEQUENCES THAT NEED FEATURES ###
    # inputFile = open(inputFilename)
    # outputFile = open(inputFilename[:-4] + '_component_k=' + msc + '.txt', 'wt')

    ComponentFeatureExtractor(site_type, msc).writeComponentTestFile(candidates_filename, candidates)

if __name__ == '__main__':
    main()
# inputSeqs = []
# lineNum = 1
# print('####\nGetting input sequences that need component features...')
# for line in input_file:
#     key, seqFull = line.split('\t')[0], line.split('\t')[1].strip()
#     seqFull += '-' + '-'.join(key.split(' ')) + '-' + str(
#         lineNum)  # add line number to make unique after trimming has occurred
#     inputSeqs.append(seqFull)
#     lineNum += 1
#
# ### GENERATE ALL POSSIBLE SUBSTRING COMBINATIONS ###
# print('####\nGenerating all possible component substrings...')
# if msc.lower() == 'full':
#     a, b = 1, 5  # make all possible k
# else:
#     a, b = int(msc.split('-')[0]), int(msc.split('-')[1])
# all_combos = set()  # will not accept duplicates
# for k in range(a, b + 1):
#     for combo_list in itertools.permutations('ATCG', k):  # all possible unique combos
#         combo_string = ''.join(combo_list)
#         all_combos.add(combo_string)
#     for combo_list in itertools.product('ATCG', repeat=k):  # all possible combos with repeated nucleotides
#         combo_string = ''.join(combo_list)
#         all_combos.add(combo_string)
# all_combos = list(sorted(all_combos))
#
#
# # function to calculate probabilities
# def getProb(freq, length, k):
#     prob = freq / (length - k + 1)
#     return prob
#
#
# # function to trim input sequences by type (ie donor vs acceptor)
# def trimSeqs(siteType, inputSeq):
#     if siteType.lower() == 'donors':  # leave out consensus dinucleotide
#         seqLeft = inputSeq[:-8]
#         seqRight = inputSeq[9:]
#     elif siteType.lower() == 'acceptors':  # leave out consensus dinucleotide
#         seqLeft = inputSeq[:-22]
#         seqRight = inputSeq[70:]
#     return seqLeft, seqRight
#
#
# ### PREPARE OUTPUT FILE ###
# out = open(input_filename[:-4] + '_component_k=' + msc + '.txt', 'wt')  # initialize output file in current directory
# out.write('Sequence\t')  # write header
# for combo in all_combos:
#     out.write('P(' + combo + ')left\tP(' + combo + ')right\t')
# out.write('\n')
#
# ### EXTRACT COMPONENT FEATURES ###
# print('####\nExtracting component features...')
# for inputSeq in inputSeqs:
#     out.write(inputSeq + '\t')
#     seqLeft, seqRight = trimSeqs(siteType, inputSeq.split('-')[0])
#     for combo in all_combos:
#         # use re.findall with lookahead assertion for overlapping matches
#         freqLeft, freqRight = len(re.findall(r'(?=(' + combo + '))', seqLeft)), len(
#             re.findall(r'(?=(' + combo + '))', seqRight))
#         if freqLeft > 0:
#             probLeft = getProb(freqLeft, len(seqLeft), len(combo))
#         else:
#             probLeft = 0
#         if freqRight > 0:
#             probRight = getProb(freqRight, len(seqRight), len(combo))
#         else:
#             probRight = 0
#         out.write(str(probLeft) + '\t' + str(probRight) + '\t')
#     out.write('\n')
# out.close()
