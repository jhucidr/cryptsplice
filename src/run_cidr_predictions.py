# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import string
import os


siteType = sys.argv[1] # for simultaneous running
if siteType == 'acceptors':
    acc_split = sys.argv[2] # to run acceptors simultaneously

filenames = []
for filename in os.listdir('.'):
    if 'simple_combined_features' in filename:
        filenames.append(filename)

if siteType == 'donors':
    to_run = []
    for i in range(19): # a split only goes up to at
        x = 'a' + string.lowercase[i]
        y = 'candidate_RefSeq_donor_split_' + x + '_simple_combined_features'
        # each combined feature file has a different date so find the right one w/list comprehension
        f = [name for name in filenames if y in name]
        to_run.append(''.join(f)) # flatten list
elif siteType == 'acceptors':
    to_run = []
    if acc_split.lower() == 'a':
        for i in range(26):
            x = 'a' + string.lowercase[i]
            y = 'candidate_RefSeq_acceptors_split_' + x + '_simple_combined_features'
            # each combined feature file has a different date so find the right one w/list comprehension
            f = [name for name in filenames if y in name]
            to_run.append(''.join(f))
    elif acc_split.lower() == 'b':
        for i in range(26):
            x = 'b' + string.lowercase[i]
            y = 'candidate_RefSeq_acceptors_split_' + x + '_simple_combined_features'
            # each combined feature file has a different date so find the right one w/list comprehension
            f = [name for name in filenames if y in name]
            to_run.append(''.join(f))
    elif acc_split.lower() == 'c':
        for i in range(7): # c split only goes up to ch
            x = 'c' + string.lowercase[i]
            y = 'candidate_RefSeq_acceptors_split_' + x + '_simple_combined_features'
            # each combined feature file has a different date so find the right one w/list comprehension
            f = [name for name in filenames if y in name]
            to_run.append(''.join(f))

print(len(to_run))

for filename_to_run in to_run:
    cmd = 'python test_model_clinvar.py ' + siteType + ' ' + filename_to_run
    print('############## Performing prediction on ' + filename_to_run + ' ##############')
    os.system(cmd)