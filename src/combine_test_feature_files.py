# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import sys
import pandas as pd
import numpy as np
import scipy
import time

import splice_site_utils


class CandidateFeatureCombiner:
    def __init__(self, siteType):
        self.siteType = siteType

    def getFileColumns(self, dataFrameColumns):
        columns_to_use = ['Sequence']

        if self.siteType == 'donors':
            a, b = 2, 4  # optimal msc
        elif self.siteType == 'acceptors':
            a, b = 1, 4  # optimal msc

        for k in range(a, b + 1):
            columns_to_use += [f for f in dataFrameColumns if (f.startswith('P') and (f.find(')') - f.find('(') == k + 1))]

        return columns_to_use

    def writeCombinedFeatureFile(self, componentFileName, positionFileName, aprFileName, outputFileName = None):
        # START FULL FEATURE DATAFRAME WITH COMPONENT FEATURES #
        print('Adding ' + componentFileName + ' to dataframe...')
        df = pd.read_csv(componentFileName, '\t', index_col=False)

        columns = df.columns
        # print(columns)

        fileColumns = self.getFileColumns(columns)
        print("columns_to_use: ", len(fileColumns))
        # print(fileColumns)
        df = df[fileColumns]

        ### ADD POS FEATURES TO FULL FEATURE DATAFRAME ###
        print('Adding ' + positionFileName + ' to dataframe...')
        pos_df = pd.read_csv(positionFileName, '\t', index_col=False)  # read tab delimited text as a csv
        pos_feature_columns = [col for col in pos_df.columns if 'f' in col or col == 'Sequence']
        pos_df_features = pos_df[pos_feature_columns]
        df = pd.merge(df, pos_df_features, on='Sequence')  # add pos features to dataframe and match by 'Sequence' column

        ### ADD APR FEATURES TO FULL FEATURE DATAFRAME ###
        print('Adding ' + aprFileName + ' to dataframe...')
        apr_df = pd.read_csv(aprFileName, '\t', index_col=False)
        apr_feature_columns = [col for col in apr_df.columns if 'f' in col or col == 'Sequence']
        apr_df_features = apr_df[apr_feature_columns]
        df = pd.merge(df, apr_df_features, on='Sequence')  # add APR features to dataframe and match by 'Sequence' column

        ### WRITE OUT ###
        if outputFileName is None:
            timestamp = splice_site_utils.getFileTimeStamp()
            outputFileName = 'candidate_RefSeq_' + self.siteType + '_simple_combined_features_' + timestamp + '.csv'

        df.to_csv(outputFileName, index=False)

        return outputFileName


def main():
    ### PROVIDE THE SCRIPT WITH FEATURE FILES EXTRACTED FROM SAME TEST DATASET ###
    siteType = sys.argv[1]
    comp_filename = sys.argv[2]
    pos_filename = sys.argv[3]
    apr_filename = sys.argv[4]


    CandidateFeatureCombiner(siteType).writeCombinedFeatureFile(comp_filename, pos_filename, apr_filename)

if __name__ == '__main__':
    main()
