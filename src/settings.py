# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#Candidate Feature Extraction Settings

#Multiscale component of interest, e.g. '1-4' is k=1~4 or 'full' for all possible k
DONORS_COMPONENT_FEATURE_EXTRACTION_MULTISCALE_VALUE = 'full'
ACCEPTOR_COMPONENT_FEATURE_EXTRACTION_MULTISCALE_VALUE = 'full'

#Examples: 'full' - Full window size, '3/5' - -3 from variant pos to +5 from variant pos
DONORS_POS_FEATURE_EXTRACTION_WINDOW_SIZE = '3/5'
ACCEPTOR_POS_FEATURE_EXTRACTION_WINDOW_SIZE = '22/1'
DONORS_APR_FEATURE_EXTRACTION_WINDOW_SIZE = '3/5'
ACCEPTOR_APR_FEATURE_EXTRACTION_WINDOW_SIZE = '22/3'

#Prediction Model Settings
MODEL_FILE_NAMES = {
    'donors': 'data/donor_models/model_SVM_RBF_nofeatselect_10foldparamopt_prob=True_combined_hs3d-nn269_donors_simple_2015-07-31.pkl',
    'acceptors': 'data/acceptor_models/model_SVM_RBF_nofeatselect_10foldparamopt_prob=True_combined_hs3d-nn269_acceptors_simple_2015-07-30.pkl'
}

#Extract from VCF settings
REF_SEQ_GENES_FILE_NAME = 'data/refseq_genes_dict.pkl'

PREDICTION_PRIORITIZATION_DB_FILE_NAME = 'data/ref_seq_db.sqllite3'

DONOR_VAR_PROB_CRYPTIC_ACTIVATION_THRESHOLD = 0.7
DONOR_VAR_PROB_WEAKENING_THRESHOLD = 0.85

ACCEPTOR_VAR_PROB_CRYPTIC_ACTIVATION_THRESHOLD = 0.6
ACCEPTOR_VAR_PROB_WEAKENING_THRESHOLD = 0.85

DELTA_VARIANT_THRESHOLD = 0.05
DELTA_CANONICAL_THRESHOLD = 0.05

VALIDATE_USER_SUPPLIED_VARIANTS = True
