# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys

donor_count, acceptor_count = 0, 0
donor_weaken, acceptor_weaken = 0, 0
donor_outcomp, acceptor_outcomp = 0, 0
donor_pseudo, acceptor_pseudo = 0, 0
donor_intron, acceptor_intron = set(), set()
donor_exon, acceptor_exon = set(), set()
donor_stop, acceptor_stop = 0, 0
donor_syn, acceptor_syn = 0, 0
donor_missense, donor_stop_vars, donor_synonymous = set(), set(), set()
acceptor_missense, acceptor_stop_vars, acceptor_synonymous = set(), set(), set()
accessions, variants = set(), set()

priority_file = open(sys.argv[1])
for line in priority_file:
    if not line.startswith('>'):
        consequence = line.split('\t')[6].strip()
        seq, accession, names = line.split('\t')[0].split('-')[0], line.split('\t')[0].split('-')[2], line.split('\t')[0].split('-')[3:]
        if len(names) == 4: # minus intron variants
            hgvs_cdna = '-'.join(names[:2])
            hgvs_pname = None
        elif len(names) == 3:
            if 'p.' not in names[1]: # minus intron vars w/o pnames
                hgvs_cdna = '-'.join(names[:2])
                hgvs_pname = None
            else: # exonic variants
                hgvs_cdna = names[0]
                hgvs_pname = names[1]
        elif len(names) == 2: # plus intron variants
            hgvs_cdna = names[0]
            hgvs_pname = None
        if len(seq) == 15:
            donor_count += 1
            if 'weaken' in consequence:
                donor_weaken += 1
            elif 'outcompete' in consequence:
                donor_outcomp += 1
            elif 'pseudoexon' in consequence:
                donor_pseudo += 1
            if '+' in hgvs_cdna or '-' in hgvs_cdna:
                donor_intron.add(hgvs_cdna)
            else:
                donor_exon.add(hgvs_cdna)
            if hgvs_pname is not None and '?' not in hgvs_pname:
                if 'ter' in hgvs_pname.lower() or 'fs' in hgvs_pname.lower():
                    donor_stop += 1
                    donor_stop_vars.add(hgvs_cdna + '-' + hgvs_pname)
                elif '=' in hgvs_pname:
                    donor_syn += 1
                    donor_synonymous.add(hgvs_cdna + '-' + hgvs_pname)
                else:
                    donor_missense.add(hgvs_cdna + '-' + hgvs_pname)
        elif len(seq) == 90:
            acceptor_count += 1
            if 'weaken' in consequence:
                acceptor_weaken += 1
            elif 'outcompete' in consequence:
                acceptor_outcomp += 1
            elif 'pseudoexon' in consequence:
                acceptor_pseudo += 1
            if '+' in hgvs_cdna or '-' in hgvs_cdna:
                acceptor_intron.add(hgvs_cdna)
            else:
                acceptor_exon.add(hgvs_cdna)
            if hgvs_pname is not None and '?' not in hgvs_pname:
                if 'ter' in hgvs_pname.lower() or 'fs' in hgvs_pname.lower():
                    acceptor_stop += 1
                    acceptor_stop_vars.add(hgvs_cdna + '-' + hgvs_pname)
                elif '=' in hgvs_pname:
                    acceptor_syn += 1
                    acceptor_synonymous.add(hgvs_cdna + '-' + hgvs_pname)
                else:
                    acceptor_missense.add(hgvs_cdna + '-' + hgvs_pname)
        accessions.add(accession)
        variants.add(hgvs_cdna)

print('len(accessions): ' + str(len(accessions)))
print('len(variants): ' + str(len(variants)))
print('-------------------------------------------')
print('total prioritized donor predictions: ' + str(donor_count))
print('-------------------------------------------')
print('donors which weaken canonical: ' + str(donor_weaken))
print('donors which outcompete canonical: ' + str(donor_outcomp))
print('donors which activate pseudoexon: ' + str(donor_pseudo))
print('intronic donors: ' + str(len(donor_intron)))
#print(donor_intron)
print('exonic donors: ' + str(len(donor_exon)))
#print(donor_exon)
print('len(donor_missense): ' + str(len(donor_missense)))
# print(donor_missense)
print('stop gain donors: ' + str(donor_stop))
print('len(donor_stop_vars): ' + str(len(donor_stop_vars)))
# print(donor_stop_vars)
print('synonymous donors: ' + str(donor_syn))
print('len(donor_synonymous): ' + str(len(donor_synonymous)))
# print(donor_synonymous)
print('-------------------------------------------')
print('total prioritized acceptor predictions: ' + str(acceptor_count))
print('-------------------------------------------')
print('acceptors which weaken canonical: ' + str(acceptor_weaken))
print('acceptors which outcompete canonical: ' + str(acceptor_outcomp))
print('acceptors which activate pseudoexon: ' + str(acceptor_pseudo))
print('intronic acceptors: ' + str(len(acceptor_intron)))
#print(acceptor_intron)
print('exonic acceptors: ' + str(len(acceptor_exon)))
#print(acceptor_exon)
print('len(acceptor_missense): ' + str(len(acceptor_missense)))
# print(acceptor_missense)
print('stop gain acceptors: ' + str(acceptor_stop))
print('len(acceptor_stop_vars): ' + str(len(acceptor_stop_vars)))
# print(acceptor_stop_vars)
print('synonymous acceptors: ' + str(acceptor_syn))
print('len(acceptor_synonymous): ' + str(len(acceptor_synonymous)))
# print(acceptor_synonymous)
