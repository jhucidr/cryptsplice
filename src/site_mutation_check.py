# def getDonorSiteIndices(seq, var_index, window_lower_limit, window_upper_limit):
#     donor_indices = []
#
#     for i in range(0, len(seq) - 1):
#         if seq[i] == "G":
#             if seq[i + 1] == "T":
#                 if var_index in range(i - window_lower_limit, i + window_upper_limit) and \
#                         (var_index != i and var_index != i + 1):
#                     donor_indices.append(i)
#
#     return donor_indices
#
#
# def getAcceptorSiteIndices(seq, var_index, window_lower_limit, window_upper_limit):
#     acceptor_indices = []
#
#     for i in range(0, len(seq) - 1):
#         if seq[i] == "A":
#             if seq[i + 1] == "G":
#                 if var_index in range(i - window_lower_limit, i + window_upper_limit) and \
#                         (var_index != i and var_index != i + 1):
#                     acceptor_indices.append(i)
#
#     return acceptor_indices


# def donorSequenceIsOutsideExonAndDoesNotMutateSite(mut_seq):
#     print(mut_seq[7:9])
#     return len(mut_seq) == 15 and mut_seq[7:9] == 'GT'
#
#
# def acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mut_seq):
#     return len(mut_seq) == 90 and mut_seq[68:70] == 'AG'

def checkDonorSNP(seq, var_index, alt):
    if seq[var_index-1] + seq[var_index] == "GT":
        return alt == "T"
    elif seq[var_index] + seq[var_index+1] == "GT":
        return alt == "G"

    return True

def checkAcceptorSNP(seq, var_index, alt):
    if seq[var_index-1] + seq[var_index] == "AG":
        return alt == "G"
    elif seq[var_index] + seq[var_index+1] == "AG":
        return alt == "A"

    return True

# def checkDonorConsensusSNP(seq, var_index, alt, gene_end):
#     return checkDonorSNP(seq, var_index, alt)
    # mut_seq = list(seq)
    # mut_seq[var_index] = alt  # change variant
    # x = var_index - 7
    # y = var_index + 8
    #
    # print(mut_seq[x:y])
    # if x > 0 and y < gene_end:
    #     return donorSequenceIsOutsideExonAndDoesNotMutateSite(mut_seq[x:y])
    # else:
    #     return False

    # # variant is G in GT; alt allele is first
    # if (alt + mut_seq[var_index + 1]) == 'GT':
    #     # donor sequences are -7, +6
    #     x = var_index - 7
    #     y = var_index + 8
    #     # check if sequence will overlap exon and if not create mutant sequence
    #     if x > 0 and y < gene_end:
    #         mutSeq_trim = mut_seq[x:y]
    #         return donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
    #
    # # variant is T in GT; alt allele is second
    # if (mut_seq[var_index - 1] + alt) == 'GT':
    #     # donor sequences are -7, +6
    #     x = var_index - 8
    #     y = var_index + 7
    #     # check if sequence will overlap exon and if not create mutant sequence
    #     if x > 0 and y < gene_end:
    #         mutSeq_trim = mut_seq[x:y]
    #         return donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
    #
    # return False


# def checkAcceptorConsensusSNP(seq, var_index, alt, gene_end):
#     mut_seq = list(seq)
#     mut_seq[var_index] = alt  # change variant
#     # variant is A in AG; alt allele is first
#     if (alt + mut_seq[var_index + 1]) == 'AG':
#         # acceptor sequences are -68, +20
#         x = var_index - 68
#         y = var_index + 22
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             return acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#
#     # variant is G in AG; alt allele is second
#     if (mut_seq[var_index - 1] + alt) == 'AG':
#         # acceptor sequences are -68, +20
#         x = var_index - 69
#         y = var_index + 21
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             return acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#
#     return False
#
#
# def checkDonorNonConsensusSNP(seq, var_index, alt, gene_end):
#     mut_seq = list(seq)  # mutate a temporary list, not original full gene sequence
#     mut_seq[var_index] = alt
#     donor_indices = getDonorSiteIndices(seq, var_index, -3, 7)
#
#     # # get indices of existing donors with the variant within -3, +5 (smallest window), leave out dints involving variant
#     # donor_indices = [x for x in donor_indices if
#     #                  var_index in range(x - 3, x + 7) and (var_index != x and var_index != x + 1)]
#     # extract -7, +6 sequence around each existing dinucleotide
#     for i in donor_indices:
#         x = i - 7
#         y = i + 8
#         # check if sequence will overlap exon and if not trim mutant sequence
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             return donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#             # assert len(mutSeq_trim) == 15, 'Length of window is incorrect!'
#             # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#             # donor_seqs.append(mutSeq_trim)
#
#     return False
#
#
# def checkAcceptorNonConsensusSNP(seq, var_index, alt, gene_end):
#     mut_seq = list(seq)  # mutate a temporary list, not original full gene sequence
#     mut_seq[var_index] = alt
#     acceptor_indices = getAcceptorSiteIndices(seq, var_index, -22, 3)
#
#     # get indices of existing acceptors with the variant within -22, +1 (smallest window), leave out dints involving variant
#     acceptor_indices = [x for x in acceptor_indices if
#                         var_index in range(x - 22, x + 3) and (var_index != x and var_index != x + 1)]
#     # extract -68, +20 sequence around each existing dinucleotide
#     for i in acceptor_indices:
#         x = i - 68
#         y = i + 22
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             return acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#             # assert len(mutSeq_trim) == 90, 'Length of window is incorrect!'
#             # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#             # acceptor_seqs.append(mutSeq_trim)
#
#     return False

def checkDonorDel(seq, var_index, alt):
    return checkDel(seq, var_index, alt, "GT")

def checkAcceptorDel(seq, var_index, alt):
    return checkDel(seq, var_index, alt, "AG")

def checkDel(seq, var_index, alt, site):
    deletion_size = len(alt)
    deleted_seq = seq[var_index:var_index + deletion_size + 1]
    return site not in deleted_seq

# def getDeletionSequence(seq, var_index, alt):
#     n = len(alt)  # length of deleted sequence
#     mut_seq = list(seq)
#     mut_seq[var_index + 1:var_index + n + 1] = ''  # delete variant
#     return mut_seq


# def checkDonorConsensusDel(seq, var_index, alt, gene_end):
#     try:
#         mut_seq = getDeletionSequence(seq, var_index, alt)
#
#         # set variables for centering sequence based on where dinucleotide is created
#         if mut_seq[var_index:var_index + 2] == 'GT':
#             x = var_index - 7
#             y = var_index + 8
#             # check if sequence will overlap exon and if not create mutant sequence
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mut_seq[x:y]
#                 if donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                     return True
#                     # assert len(mutSeq_trim) == 15, 'Length of window is incorrect for: ' + mutSeq_trim
#                     # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#
#     except IndexError:
#         print('The candidate variant overlaps the exon!')
#
#     return False
#
#
# def checkAcceptorConsensusDel(seq, var_index, alt, gene_end):
#     try:
#         mut_seq = getDeletionSequence(seq, var_index, alt)
#
#         # set variables for centering sequence based on where dinucleotide is created
#         if mut_seq[var_index:var_index + 2] == 'AG':
#             x = var_index - 68
#             y = var_index + 22
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mut_seq[x:y]
#                 if acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                     return True
#                     # assert len(mutSeq_trim) == 90, 'Length of window is incorrect: ' + mutSeq_trim
#                     # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#                     # acceptor_seqs.append(mutSeq_trim)
#
#     except IndexError:
#         print('The candidate variant overlaps the exon!')
#
#     return False
#
#
# def checkDonorNonConsensusDEL(seq, var_index, alt, gene_end):
#     n = len(alt)  # length of inserted sequence
#     mut_seq = getDeletionSequence(seq, var_index, alt)
#     donor_indices = getDonorSiteIndices(seq, var_index, n - 3, 7)
#
#     # get indices of existing donors with the deletion within -3, +5, leave out dints involving the deletion
#     # donor_indices = [x for x in donor_indices if
#     #                  var_index in range(x - n - 3, x + 7) and x not in range(var_index - 1, var_index + n + 1)]
#     # extract -7, +6 sequence around the dinucleotide
#     for i in donor_indices:
#         if var_index <= i:  # deletion is 5' of dint
#             i -= n  # reset index of dint for deletion
#             x = i - 7
#             y = i + 8
#         else:  # deletion is 3' of dint, won't change dint index
#             x = i - 7
#             y = i + 8
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             if donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                 return True
#                 # assert len(mutSeq_trim) == 15, 'Length of window is incorrect for: ' + mutSeq_trim
#                 # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#                 # donor_seqs.append(mutSeq_trim)
#
#     return False
#
# def checkAcceptorNonConsenusDEL(seq, var_index, alt, gene_end):
#     n = len(alt)  # length of inserted sequence
#     mut_seq = getDeletionSequence(seq, var_index, alt)
#     acceptor_indices = getAcceptorSiteIndices(seq, var_index, n - 22, 3)
#
#     # get indices of existing acceptors with any part of the insertion within -22, +1, leave out dints split by variant
#     # acceptor_indices = [x for x in acceptor_indices if
#     #                     var_index in range(x - n - 22, x + 3) and x not in range(var_index - 1, var_index + n + 1)]
#     # extract -68, +20 sequence around the dinucleotide
#     for i in acceptor_indices:
#         if var_index <= i:  # insertion is 5' of dint
#             i -= n  # reset index of dint for deletion
#             x = i - 68
#             y = i + 22
#         else:  # deletion is 3' of dint, won't change dint index
#             x = i - 68
#             y = i + 22
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             if acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                 return True
#                 # assert len(mutSeq_trim) == 90, 'Length of window is incorrect for: ' + mutSeq_trim
#                 # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#                 # acceptor_seqs.append(mutSeq_trim)
#
#     return False


# def getInsertionSequence(seq, var_index, alt):
#     mut_seq = list(seq)
#     mut_seq.insert(var_index, alt)  # insert variant
#     return mut_seq

def checkDonorINSorDUP(seq, var_index, alt):
    if seq[var_index] + seq[var_index + 1] == "GT":
        return seq[var_index] + alt[0] == "GT"

    return True

def checkAcceptorINSorDUP(seq, var_index, alt):
    if seq[var_index] + seq[var_index + 1] == "AG":
        return seq[var_index] + alt[0] == "AG"

    return True

# def checkDonorConsensusINSorDUP(seq, var_index, alt, gene_end):
#     try:  # won't index if var_index too close to gene end
#         n = len(alt)  # length of inserted sequence
#         mutSeq_full = getInsertionSequence(seq, var_index, alt)
#
#         if (mutSeq_full[var_index - 1] + alt[0]) == 'GT':
#             x = var_index - 8
#             y = var_index + 7
#             # check if sequence will overlap exon and if not create mutant sequence
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mutSeq_full[x:y]
#                 return donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#                 # assert len(mutSeq_trim) == 15, 'Length of window is incorrect: ' + mutSeq_trim
#                 # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#                 # donor_seqs.append(mutSeq_trim)
#         if (alt[-1] + mutSeq_full[var_index + n]) == 'GT':
#             x = var_index + n - 8
#             y = var_index + n + 7
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mutSeq_full[x:y]
#                 return donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim)
#                 # assert len(mutSeq_trim) == 15, 'Length of window is incorrect for: ' + mutSeq_trim
#                 # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#                 # donor_seqs.append(mutSeq_trim)
#
#     except IndexError:
#         print('The candidate variant overlaps the exon!')
#
#     return False
#
#
# def checkAcceptorConsensusINSorDUP(seq, var_index, alt, gene_end):
#     try:  # won't index if var_index too close to gene end
#         n = len(alt)  # length of inserted sequence
#         mutSeq_full = getInsertionSequence(seq, var_index, alt)
#
#         if (mutSeq_full[var_index - 1] + alt[0]) == 'AG':
#             x = var_index - 69
#             y = var_index + 21
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mutSeq_full[x:y]
#                 if acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                     return True
#                     # assert len(mutSeq_trim) == 90, 'Length of window is incorrect: ' + mutSeq_trim
#                     # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#                     # acceptor_seqs.append(mutSeq_trim)
#         if (alt[-1] + mutSeq_full[var_index + n]) == 'AG':
#             x = var_index + n - 69
#             y = var_index + n + 21
#             if x > 0 and y < gene_end:
#                 mutSeq_trim = mutSeq_full[x:y]
#                 if acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                     return True
#                     # assert len(mutSeq_trim) == 90, 'Length of window is incorrect: ' + mutSeq_trim
#                     # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#                     # acceptor_seqs.append(mutSeq_trim)
#
#     except IndexError:
#         print('The candidate variant overlaps the exon!')
#
#     return False
#
#
# def getINSDonorSiteIndices(seq, var_index, insert_len, lower_window, upper_window):
#     donor_indices = []
#
#     for i in range(0, len(seq)):
#         if seq[i] == "G":
#             if seq[i + 1] == "T":
#                 if var_index in range((i + insert_len) - lower_window, i + upper_window) and \
#                                 var_index != i + 1:
#                     donor_indices.append(i)
#
#     return donor_indices
#
#
# def checkDonorNonCensensusINSorDUP(seq, var_index, alt, gene_end):
#     n = len(alt)  # length of inserted sequence
#     mut_seq = getInsertionSequence(seq, var_index, alt)
#     donor_indices = getINSDonorSiteIndices(seq, var_index, n, 3 - (n - 1), 7)
#
#     # get indices of existing donors with any part of the insertion within -3, +5, leave out dints split by variant
#     # donor_indices = [x for x in donor_indices if
#     #                  var_index in range((x + n) - 3 - (n - 1), x + 7) and var_index != x + 1]
#     # extract -7, +6 sequence around the dinucleotide
#     for i in donor_indices:
#         if var_index <= i:  # insertion is 5' of dint
#             i += n  # reset index of dint for insertion
#             x = i - 7
#             y = i + 8
#         else:  # insertion is 3' of dint, won't change dint index
#             x = i - 7
#             y = i + 8
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             if donorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                 return True
#                 # assert len(mutSeq_trim) == 15, 'Length of window is incorrect for: ' + mutSeq_trim
#                 # assert mutSeq_trim[7:9] == 'GT', 'Sequence not centered around GT for: ' + mutSeq_trim
#                 # donor_seqs.append(mutSeq_trim)
#
#     return False
#
#
# def getINSAcceptorSiteIndices(seq, var_index, insert_len, lower_window, upper_window):
#     acceptor_indices = []
#
#     for i in range(0, len(seq)):
#         if seq[i] == "A":
#             if seq[i + 1] == "G":
#                 if var_index in range((i + insert_len) - lower_window, i + upper_window) and \
#                                 var_index != i + 1:
#                     acceptor_indices.append(i)
#
#     return acceptor_indices
#
#
# def checkAcceptorNonCensensusINSorDUP(seq, var_index, alt, gene_end):
#     n = len(alt)  # length of inserted sequence
#     mut_seq = getInsertionSequence(seq, var_index, alt)
#     acceptor_indices = getINSAcceptorSiteIndices(seq, var_index, n, 22 - (n - 1), 3)
#
#     # get indices of existing acceptors with any part of the insertion within -22, +1, leave out dints split by variant
#     # acceptor_indices = [x for x in acceptor_indices if
#     #                     var_index in range((x + n) - 22 - (n - 1), x + 3) and var_index != x + 1]
#     # extract -68, +20 sequence around the dinucleotide
#     for i in acceptor_indices:
#         if var_index <= i:  # insertion is 5' of dint
#             i += n  # reset index of dint for insertion
#             x = i - 68
#             y = i + 22
#         else:  # insertion is 3' of dint, won't change dint index
#             x = i - 68
#             y = i + 22
#         if x > 0 and y < gene_end:
#             mutSeq_trim = mut_seq[x:y]
#             if acceptorSequenceIsOutsideExonAndDoesNotMutateSite(mutSeq_trim):
#                 return True
#                 # assert len(mutSeq_trim) == 90, 'Length of window is incorrect for: ' + mutSeq_trim
#                 # assert mutSeq_trim[68:70] == 'AG', 'Sequence not centered around AG for: ' + mutSeq_trim
#                 # acceptor_seqs.append(mutSeq_trim)
#
#     return False
