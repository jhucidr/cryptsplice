# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import pickle
import re

clinvar_file = open('variant_summary.txt')
refseq_genes_file = open('refseq_genes_fasta.fa')
refseq_exons_file = open('refseq_exons.bed')
symbol_file = open('hgnc_refseq.txt')


### ORGANIZE HGNC SYMBOLS AND MATCHING REFSEQ ACCESSION NUMBERS INTO A DICTIONARY ###
symbol_dict = {}

for line in symbol_file:
	try:
		hgnc, accession = line.split('\t')[0], line.split('\t')[1].strip()
		symbol_dict[accession] = hgnc
	except IndexError:
		pass

print('len(symbol_dict): ' + str(len(symbol_dict.keys())))
pickle.dump(symbol_dict, open('hgnc_refseq_symbol_dict.pkl', 'wb'))

### ORGANIZE CLINVAR VARIANTS INTO A DICTIONARY ###
clinvar_dict = {}

for line in clinvar_file:
	if not line.startswith('#'):
		if ('single nucleotide variant' in line) and ('GRCh37' in line) and ('athogenic' in line) and ('enign' not in line):
			accession = line.split('\t')[2].split(':')[0].split('.')[0]
			chrom, coord, alt = line.split('\t')[13], int(line.split('\t')[14]), line.split('\t')[26]
			try:
				# won't split if non hgvs name in this column
				hgvs_names = line.split('\t')[2].split(':')[1]
				clinvar_dict[hgvs_names] = [accession, chrom, coord, alt]
			except IndexError:
				pass

print('len(clinvar_dict): ' + str(len(clinvar_dict.keys())))
pickle.dump(clinvar_dict, open('clinvar_dict.pkl', 'wb'))

### ORGANIZE REFSEQ EXON BED INTO A DICTIONARY ###
exon_dict = {}

for line in refseq_exons_file:
	if line.strip().endswith('+'): # plus strand genes only
		chrom, start, stop, name = line.split('\t')[0], int(line.split('\t')[1]), int(line.split('\t')[2]), line.split('\t')[3]
		accession = '_'.join(name.split('_')[0:2])
		exon_coords = [start, stop]
		if accession in exon_dict.keys():
			exon_dict[accession].append(exon_coords)
		else:
			exon_dict[accession] = [exon_coords]

print('len(exon_dict): ' + str(len(exon_dict.keys())))
pickle.dump(exon_dict, open('refseq_exons_dict.pkl', 'wb'))

### ORGANIZE REFSEQ GENES INTO A DICTIONARY ###
refseq_dict = {}

for line in refseq_genes_file:
	if line.startswith('>'):
		# this regex matches every possible fasta header in the refseq file--all lines must be added in order!
		match = re.search(r'>hg19_refGene_(N(?:M|R)_[0-9]+)\srange=(chr\w+:[0-9]+-[0-9]+)\s5\'pad=0\s3\'pad=0\sstrand=((?:\+|-))\srepeatMasking=none', line)
		accession, locus, strand = match.group(1), match.group(2), match.group(3)
		refseq_dict[accession] = [locus, strand]
	else:
		# add each line of sequence to list
		refseq_dict[accession].append(line.strip())

for key, value in refseq_dict.items():
	# plus strand genes only
	if value[1] is '+':
		try:
			# match won't be found for weird contigs
			match = re.search(r'chr(\w{1,2}):([0-9]+)-([0-9]+)', value[0])
			chrom, start, stop = match.group(1), int(match.group(2)), int(match.group(3))
			# make sequence concatenated into one string from value
			seq = ''.join(value[2:])
			refseq_dict[key] = [chrom, start, stop, seq]
		except AttributeError:
			# remove from dict
			del refseq_dict[key]
	else:
		del refseq_dict[key]

print('len(refseq_dict): ' + str(len(refseq_dict.keys())))
pickle.dump(refseq_dict, open('refseq_genes_dict.pkl', 'wb'))