# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os
import sys
import collections

import splice_site_utils


class APRFeatureExtractor:
    dintList = ['AA', 'AT', 'AC', 'AG', 'TA', 'TT', 'TC', 'TG', 'CA', 'CT', 'CC', 'CG', 'GA', 'GT', 'GC', 'GG']

    def __init__(self, siteType, windowSize):
        self.siteType = siteType
        self.windowSize = windowSize

    ### CREATE DICT OF INPUT SEQUENCES THAT NEED FEATURES EXTRACTED ###
    """
    Purpose: To trim sequences based on origin file and site type (i.e. donor, acceptor).
    Arguments: Sequence to be trimmed, splice site type, window size of interest, filename that contains "true" or "false".
    Returns: Trimmed splice sequence.
    """
    def trimSeq(self, sequence):
        return splice_site_utils.trimSeq(sequence, self.siteType, self.windowSize)
    # def trimSeq(self, sequence):
    #     if self.windowSize == 'full':
    #         trimmedSeq = sequence.strip()
    #     else:
    #         tokens = self.windowSize.split('/')
    #         left, right = int(tokens[0]), int(tokens[1])
    #         if self.siteType == 'donors':
    #             if left == 1:
    #                 x = 6
    #             elif left == 3:
    #                 x = 4
    #             elif left == 5:
    #                 x = 2
    #             if right == 3:
    #                 y = 12
    #             elif right == 5:
    #                 y = 14
    #             elif right == 7:
    #                 y, z = 15, 'N' # add artificial base to fill window
    #             elif right == 9:
    #                 y, z = 15, 'NNN'
    #         elif self.siteType == 'acceptors':
    #             if left == 20:
    #                 x = 48
    #             elif left == 22:
    #                 x = 46
    #             elif left == 24:
    #                 x = 48
    #             if right == 1:
    #                 y = 71
    #             elif right == 3:
    #                 y = 73
    #             elif right == 5:
    #                 y = 75
    #         try:
    #             trimmedSeq = sequence[x:y] + z
    #         except NameError: # if z is not defined do this below
    #             trimmedSeq = sequence[x:y]
    #     return trimmedSeq

    def trimSequences(self, candidates):
        inputSeqs = collections.OrderedDict() # keep key as seqFull and value as trimmedSeq
        lineNum = 1  # counter for duplicate sequences
        seqLength = 0

        for candidate in candidates:
            seqFull = candidate.seq
            trimmedSeq = self.trimSeq(seqFull)
            seqLength = len(trimmedSeq) # save for creating feature column names
            key = seqFull + '-' + candidate.seq_id + '-' + str(lineNum) # add line number to make unique after trimming has occurred
            inputSeqs[key] = trimmedSeq
            lineNum += 1

        return inputSeqs, seqLength

    # def trimSequences(self, candidates):
    #     inputSeqs = collections.OrderedDict() # keep key as seqFull and value as trimmedSeq
    #     lineNum = 1 # counter for duplicate sequences
    #     seqLength = 0
    #
    #     for candidate in candidates:
    #         key = candidate[3]
    #         seqFull = candidate[1]
    #         trimmedSeq = self.trimSeq(seqFull)
    #         seqLength = len(trimmedSeq) # save for creating feature column names
    #         seqFull += '-' + '-'.join(key.split(' ')) + '-' + str(lineNum) # add line number to make unique after trimming has occurred
    #         inputSeqs[seqFull] = trimmedSeq
    #         lineNum += 1
    #
    #     return inputSeqs, seqLength

    ### CREATE LIST OF FEATURE COLUMN NAMES ###
    def getFeatureColumnHeader(self, seqLength):
        headerList = [] # create beauteous list of column headers
        for n in range(1, seqLength): # sequence length minus 1 is number of positions
            for dint in self.dintList:
                headerList.append('f' + str(n) + '(' + dint + ')')

        return headerList

    def writeAPRFeatures(self, inputSequenceDict, seqLength, outFile, headerList): # a function to write the features
        outFile.write('Sequence\t')
        outFile.write('\t'.join(headerList) + '\n')
        for inputSeqFull, inputTrimmedSeq in inputSequenceDict.items():
            outFile.write(inputSeqFull + '\t')
            seqBases = list(inputTrimmedSeq)
            for i in range(0, seqLength - 1): # iterate by two overlapping nucleotides
                j = i + 1
                inputDint = inputTrimmedSeq[i] + inputTrimmedSeq[j] # make dinucleotide from input sequence
                for dint in self.dintList:
                    if 'N' in inputDint:
                        outFile.write('NA')
                    elif inputDint == dint:
                        outFile.write('1')
                    else:
                        outFile.write('0')
                    outFile.write('\t')
            outFile.write('\n')
        outFile.close()

    def extractAndWriteAPRFeaturesFile(self, candidate_file_name, candidates, outputFilename = None):
        inputSeqs, seqLength = self.trimSequences(candidates)
        columnHeaders = self.getFeatureColumnHeader(seqLength)

        if outputFilename is None:
            baseCandidateFileName = os.path.basename(candidate_file_name)
            formattedWindowSize = splice_site_utils.formatWindowSizeString(self.windowSize)
            outputFilename = baseCandidateFileName[:-4] + '_' + formattedWindowSize + '_apr_simple.txt'

        outputFile = open(outputFilename, 'wt')

        self.writeAPRFeatures(inputSeqs, seqLength, outputFile, columnHeaders)

        return outputFilename

def main():
    ### COMMAND LINE ARGUMENTS ###
    siteType = sys.argv[1] # specify "donors" or "acceptors"
    assert siteType == 'donors' or siteType == 'acceptors', 'Please specifiy splice site type as donors or acceptors in first argument.'
    input_filename = sys.argv[2] # cleaned true positives input
    windowSize = input('Enter the window size of interest, e.g. +3,-5 is \'3/5\' or \'full\' for full window: ')
    candidates = splice_site_utils.getCandidates(input_filename)

    APRFeatureExtractor(siteType, windowSize).extractAndWriteAPRFeaturesFile(input_filename, candidates)

if __name__ == '__main__':
    main()