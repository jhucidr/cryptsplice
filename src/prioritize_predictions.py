# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import division

import difflib
import sys
import time

import settings
from splice_site_utils import getPredictions, getCandidates, getDeltaCanon, getDeltaVar
from ref_seq_database import RefSeqDatabase

"""
This prioritization algorithm flags variants most likely to disrupt normal mRNA processing based on probability estimates from prediction
and distance from canonical splice sites. There are two overall scenarios: cryptic activation and canonical splice site weakening. These
can be further subdivided by distance from canonical splice sites. Cryptic splice activation may occur in the deep intron or near the
canonical splice site. Each scenario may be the result of a variant creating a novel dinucleotide or altering the splice potential of
an existing dinucleotide, each event is assessed quantitatively by the calculation of deltaVariant and deltaCanonical.
"""


class PrioritizePredicitions:
    outputFile = None
    out_count = 0
    out_donor_count = 0  # number of prioritized donor predictions
    out_acceptor_count = 0  # number of prioritized acceptor predictions
    nobed_count = 0  # number of variants with an accession for which don't have an exon bed (will be multiple vars in same accession)
    nowt_count = 0  # number of consensus variants near a canon exon that couldn't find a wt prediction
    nowt_exons = set()  # track which accessions may be missing canonical wt predictions
    unknown_accessions = set()

    def __init__(self, dVthreshold, dCthreshold):
        self.dVthreshold = dVthreshold
        self.dCthreshold = dCthreshold
        self.refSeqDB = RefSeqDatabase()

    #
    # def isDeltaVarAboveThreshold(self, deltaVar):
    #     return abs(deltaVar) >= self.dVthreshold
    #
    # def isDeltaCanonAboveThreshold(self, deltaCanon):
    #     return abs(deltaCanon) >= self.dCthreshold

    def processOutcompetingCanonicalDonorVariant(self, exons, n, var_chr, var_coord, var_seq, var_prob, seqID,
                                                 accession, donor_candidates):
        # get only candidates with same coordinate
        location = var_chr + ':' + str(var_coord)
        candidates = [x for x in donor_candidates if location == x[0]]
        # iterate to match variant sequence to sequence in candidate file
        for candidate in candidates:
            if var_seq == candidate[1]:
                # variant does not create consensus GT, get deltaVar
                if 'non' in candidate[2]:
                    # iterate through all coordinates +/- 15 bp of the variant
                    for i in range(var_coord - 15, var_coord + 16):
                        wt_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession, var_chr, i)
                        if wt_prediction:
                            wt_seq = wt_prediction[0]  # retrieve wt sequence
                            compare = difflib.SequenceMatcher(None, var_seq,
                                                              wt_seq)  # are they the same?
                            # same sequence with one base different
                            if compare.ratio() == 14 / 15:
                                wt_prob = wt_prediction[1]
                                canon_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession,
                                                                                            var_chr, exons[n][1] + 1)
                                if canon_prediction:
                                    canon_prob = canon_prediction[1]
                                    deltaVar = getDeltaVar(var_prob, wt_prob)
                                    # soften canon_prob by subtracting 0.1 to capture known positives
                                    deltaCanon = getDeltaCanon(var_prob, (canon_prob - 0.1))
                                    # wt CANNOT be canonical donor for outcompeting
                                    if i != exons[n][1] + 1:
                                        if abs(deltaVar) >= self.dVthreshold \
                                                and abs(deltaCanon) >= self.dCthreshold:
                                            self.out_donor_count += 1
                                            self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                                   str(round(wt_prob, 3)), str(round(deltaVar, 3)),
                                                                   str(round(canon_prob, 3)),
                                                                   str(round(deltaCanon, 3)),
                                                                   'outcompetes canonical donor')
                                    else:
                                        self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                               str(round(wt_prob, 3)), str(round(deltaVar, 3)),
                                                               str(round(canon_prob, 3)),
                                                               str(round(deltaCanon, 3)),
                                                               'NA',
                                                               'wild type cannot be canonical donor for outcompeting')
                                else:
                                    self.nowt_count += 1
                                    self.nowt_exons.add(
                                        accession + '-' + var_chr + ':' + str(exons[n][1] + 1))
                                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                           str(round(wt_prob, 3)), 'NA',
                                                           'NA', 'NA', 'NA',
                                                           'no canonical wild type data found in database')

                # variant DOES create consensus GT, proceed to deltaCanon
                else:
                    canon_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession, var_chr, exons[n][1] + 1)
                    if canon_prediction:
                        canon_prob = canon_prediction[1]
                        # soften canon_prob by subtracting 0.1 to capture known positives
                        deltaCanon = getDeltaCanon(var_prob, (canon_prob - 0.1))
                        if deltaCanon >= self.dCthreshold:
                            self.out_donor_count += 1
                            self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                                   str(round(canon_prob, 3)), str(round(deltaCanon, 3)),
                                                   'outcompetes canonical donor')
                    else:
                        self.nowt_count += 1
                        self.nowt_exons.add(accession + '-' + var_chr + ':' + str(exons[n][1] + 1))
                        self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                               'NA', 'NA', 'NA', 'NA', 'NA',
                                               'no canonical wild type data found in database')

    def processDeepIntronicCrypticDonorVariant(self, var_chr, var_coord, var_seq, var_prob, seqID, accession,
                                               donor_candidates):
        # get only candidates with same coordinate
        location = var_chr + ':' + str(var_coord)
        candidates = [x for x in donor_candidates if location == x[0]]
        # iterate to find same candidate sequence
        for candidate in candidates:
            if var_seq == candidate[1]:

                # variant does not create consensus GT, get deltaVar
                if 'non' in candidate[2]:
                    # iterate through all coordinates +/- 15 bp of the variant
                    for i in range(var_coord - 15, var_coord + 16):
                        # cat with accession number and chr of relevant gene and search these names in wt dict
                        wt_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession, var_chr, i)
                        if wt_prediction:
                            wt_seq = wt_prediction[0]  # retrieve wt sequence
                            compare = difflib.SequenceMatcher(None, var_seq,
                                                              wt_seq)  # compare sequences
                            # same sequence with one base different
                            if compare.ratio() == 14 / 15:
                                wt_prob = wt_prediction[1]
                                # calculate delta
                                deltaVar = getDeltaVar(var_prob, wt_prob)
                                if abs(deltaVar) >= self.dVthreshold:
                                    self.out_donor_count += 1
                                    self.writeToOutputFile(seqID, str(round(var_prob, 3)), str(round(wt_prob, 3)),
                                                           str(round(deltaVar, 3)), 'NA', 'NA',
                                                           'pseudoexon inclusion')

                # variant DOES create consensus GT, no deltaVar or deltaCanon possible
                else:
                    self.out_donor_count += 1
                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                           'NA', 'NA', 'NA', 'NA',
                                           'pseudoexon inclusion')

    def processCrypticActivationDonor(self, exons, var_chr, var_coord, var_seq, var_prob, seqID, accession,
                                      donor_candidates):
        variant_processed = False

        for n in range(len(exons) - 1):  # -1 to avoid the last exon
            # OUTCOMPETING CANONICAL VARIANTS: does variant occur within 60 bp of a canonical donor but not directly inside it
            if var_coord in range(exons[n][1] - 60, exons[n][1] - 1) or var_coord in range(exons[n][1] + 3,
                                                                                           exons[n][1] + 61):
                self.processOutcompetingCanonicalDonorVariant(exons, n, var_chr, var_coord, var_seq, var_prob, seqID,
                                                              accession, donor_candidates)
                variant_processed = True

            # DEEP INTRONIC CRYPTIC DONORS: does variant occur +100 bp of current exon and -100bp of next exon?
            elif exons[n][1] + 100 <= var_coord <= exons[n + 1][0] - 100:
                self.processDeepIntronicCrypticDonorVariant(var_chr, var_coord, var_seq, var_prob, seqID, accession,
                                                            donor_candidates)
                variant_processed = True

        if not variant_processed:
            self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                   'NA', 'NA', 'NA',
                                   'cryptic activation donor: variant neither occurs within 60bp of a canonical donor or within +/-100bp between exons')

    def processWeakeningOfCanonicalDonorVariant(self, exons, var_chr, var_coord, var_seq, var_prob,
                                                seqID, accession):
        variant_in_range = False

        for n in range(len(exons) - 1):
            exon = exons[n]
            exons_end = exon[1]
            # does variant occur within the canonical donor?
            if var_coord in range(exons_end - 7, exons_end + 8):
                # iterate through all coordinates -7,+6 bp of the variant
                # for i in range(var_coord - 7, var_coord + 8):
                # check if sequence is same if coordinate has a wt prediction
                variant_in_range = True
                i = exons_end + 1
                wt_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession, var_chr, i)
                if wt_prediction:
                    # and i == exons[n][1] + 1:
                    wt_seq = wt_prediction[0]
                    compare = difflib.SequenceMatcher(None, var_seq, wt_seq)  # compare sequences
                    # same sequence with one base different
                    if compare.ratio() == 14 / 15:
                        canon_prediction = self.refSeqDB.getWildTypeDonorPrediction(accession, var_chr,
                                                                                    exons[n][1] + 1)

                        if canon_prediction:
                            canon_prob = canon_prediction[1]
                            # calculate delta (deltaVar is same as deltaCanon here but deltaCanon is more relevant)
                            deltaCanon = getDeltaCanon(var_prob, canon_prob)
                            # the change in prob must reach threshold and the deltaCanon must be negative for weakening
                            if abs(deltaCanon) >= self.dCthreshold and deltaCanon < 0:
                                self.out_donor_count += 1
                                self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                                       str(round(canon_prob, 3)), str(round(deltaCanon, 3)),
                                                       'weakens canonical donor')
                        else:
                            self.nowt_count += 1
                            self.nowt_exons.add(accession + '-' + var_chr + ':' + str(exons[n][1] + 1))
                            self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                   str(round(wt_prediction[1], 3)), 'NA',
                                                   'NA', 'NA', 'NA',
                                                   'no canonical wild type data found in database')
                else:
                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                       'NA', 'NA',
                                       'NA', 'NA', 'NA',
                                       'no wild type data found in database')

        if not variant_in_range:
            self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                   'NA', 'NA',
                                   'NA', 'NA', 'NA',
                                   'weakening canonical donor: variant is not within range of any exon donor site')

    def prioritizeDonorPredictions(self, donor_var_predictions, donor_candidates):
        for var_data in donor_var_predictions:
            var_chr, var_coord = var_data[0], var_data[1]
            accession, var_seq, var_prob, seqID = var_data[2], var_data[3], var_data[4], var_data[5]
            exons = self.refSeqDB.getGeneExonsCoordinates(accession)

            if exons:
                # CRYPTIC ACTIVATION OF DONORS #
                if var_prob >= settings.DONOR_VAR_PROB_CRYPTIC_ACTIVATION_THRESHOLD:  # only proceed with true predictions greater than the weakest true canonical donor
                    self.processCrypticActivationDonor(exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                       accession, donor_candidates)

                # WEAKENING OF CANONICAL DONORS #
                if var_prob <= settings.DONOR_VAR_PROB_WEAKENING_THRESHOLD:  # weakened canonical donors can still have a high prob and true classification
                    self.processWeakeningOfCanonicalDonorVariant(exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                                 accession)
            else:
                self.nobed_count += 1
                self.unknown_accessions.add(accession)
                self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                       'NA', 'NA', 'NA',
                                       'Accession ' + accession + ' not in database')

    def processOutcompetesCanonicalAcceptorVariant(self, exons, n, var_chr, var_coord, var_seq, var_prob, seqID,
                                                   accession, acceptor_candidates):
        # get only candidates with same coordinate
        location = var_chr + ':' + str(var_coord)
        candidates = [x for x in acceptor_candidates if location == x[0]]
        # iterate to find same candidate sequence
        for candidate in candidates:
            if var_seq == candidate[1]:

                # variant does not create consensus AG, get deltaVar
                if 'non' in candidate[2]:
                    # iterate through all coordinates +/- 20 bp of the variant to find wt prediction
                    for i in range(var_coord - 20, var_coord + 21):
                        # cat with accession number and chr of relevant gene and search these names in wt dict
                        # check if sequence is same if coordinate has a wt prediction
                        wt_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession, var_chr, i)
                        # if acc_loc in self.acceptor_wt_predictions:
                        if wt_prediction:
                            wt_seq = wt_prediction[0]
                            compare = difflib.SequenceMatcher(None, var_seq,
                                                              wt_seq)  # compare sequences
                            # same sequence with one base different
                            if compare.ratio() == 89 / 90:
                                wt_prob = wt_prediction[1]
                                canon_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession, var_chr,
                                                                                               exons[n][0] - 1)

                                if canon_prediction:
                                    canon_prob = canon_prediction[1]
                                    # # wt seq can't be canonical acceptor for outcompeting
                                    # if i != exons[n][0] - 1:
                                    deltaVar = getDeltaVar(var_prob, wt_prob)
                                    # soften canon_prob by subtracting 0.1 to capture known positives
                                    deltaCanon = getDeltaCanon(var_prob, (canon_prob - 0.1))
                                    if abs(deltaVar) >= self.dVthreshold and abs(deltaCanon) >= self.dCthreshold:
                                        self.out_acceptor_count += 1
                                        self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                               str(round(wt_prob, 3)), str(round(deltaVar, 3)),
                                                               str(round(canon_prob, 3)),
                                                               str(round(deltaCanon, 3)),
                                                               'outcompetes canonical acceptor')
                                else:
                                    self.nowt_count += 1
                                    self.nowt_exons.add(
                                        accession + '-' + var_chr + ':' + str(exons[n][0] - 1))
                                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                           str(round(wt_prediction[1], 3)), 'NA',
                                                           'NA', 'NA', 'NA',
                                                           'no canonical wild type data found in database')

                # variant DOES create consensus AG, proceed to deltaCanon
                else:
                    canon_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession,
                                                                                   var_chr,
                                                                                   exons[n][0] - 1)
                    if canon_prediction:
                        canon_prob = canon_prediction[1]
                        # soften canon_prob by subtracting 0.1 to capture known positives
                        deltaCanon = getDeltaCanon(var_prob, (canon_prob - 0.1))
                        if abs(deltaCanon) >= self.dCthreshold:
                            self.out_acceptor_count += 1
                            self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                                   str(round(canon_prob, 3)), str(round(deltaCanon, 3)),
                                                   'outcompetes canonical acceptor')
                    else:
                        self.nowt_count += 1
                        self.nowt_exons.add(accession + '-' + var_chr + ':' + str(exons[n][0] - 1))
                        self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                               'NA', 'NA',
                                               'NA', 'NA', 'NA',
                                               'no canonical wild type data found in database')

    def processDeepIntronicCrypticAcceptorVariant(self, var_chr, var_coord, var_seq, var_prob, seqID,
                                                  accession, acceptor_candidates):
        # get only candidates with same coordinate
        location = var_chr + ':' + str(var_coord)
        candidates = [x for x in acceptor_candidates if location == x[0]]
        # iterate to find same candidate sequence
        for candidate in candidates:
            if var_seq == candidate[1]:

                # variant does not create consensus AG, get deltaVar
                if 'non' in candidate[2]:
                    # iterate through all coordinates +/- 60 bp of the variant
                    for i in range(var_coord - 60, var_coord + 61):
                        wt_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession, var_chr, i)
                        if wt_prediction:
                            wt_seq = wt_prediction[0]  # retrieve wt sequence
                            compare = difflib.SequenceMatcher(None, var_seq,
                                                              wt_seq)  # compare sequences
                            # same sequence with one base different, use this i as wt coord
                            if compare.ratio() == 89 / 90:
                                wt_prob = wt_prediction[1]
                                deltaVar = getDeltaVar(var_prob, wt_prob)
                                if abs(deltaVar) >= self.dVthreshold:
                                    self.out_acceptor_count += 1
                                    self.writeToOutputFile(seqID, str(round(var_prob, 3)), str(round(wt_prob, 3)),
                                                           str(round(deltaVar, 3)), 'NA', 'NA',
                                                           'pseudoexon inclusion')

                # variant does create consensus AG, no deltaVar possible
                else:
                    self.out_acceptor_count += 1
                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                           'NA', 'NA', 'NA', 'NA',
                                           'pseudoexon inclusion')

    def processCrypticActivationAcceptorVariant(self, exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                accession, acceptor_candidates):
        variant_processed = False

        for n in range(1, len(exons)):  # start at one to avoid first canonical exon acceptor
            # OUTCOMPETING CANONICAL VARIANTS: does variant occur within 60 bp of canonical acceptor but not directly inside it?
            if var_coord in range(exons[n][0] - 60, exons[n][0] - 3) or var_coord in range(exons[n][0] + 1,
                                                                                           exons[n][0] + 61):
                self.processOutcompetesCanonicalAcceptorVariant(exons, n, var_chr, var_coord, var_seq, var_prob,
                                                                seqID, accession, acceptor_candidates)
                variant_processed = True

            # DEEP INTRONIC CRYPTIC ACCEPTORS: does variant occur +100 bp of current exon and -100bp of next exon?
            elif exons[n - 1][1] + 100 <= var_coord <= exons[n][0] - 100:
                self.processDeepIntronicCrypticAcceptorVariant(var_chr, var_coord, var_seq, var_prob, seqID,
                                                               accession, acceptor_candidates)
                variant_processed = True

        if not variant_processed:
            self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                   'NA', 'NA', 'NA',
                                   'cryptic activation acceptor: variant neither occurs within 60bp of a canonical acceptor or within +/-100bp between exons')

    def processWeakingOfCanonicalAcceptorVariant(self, exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                 accession):
        variant_in_range = False

        for n in range(1, len(exons)):
            exon = exons[n]
            exon_start = exon[0]

            # does variant occur within 22 bp downstream of AG, 3 bp upstream of AG, and not in the first exon?
            if var_coord in range(exon_start - 22, exon_start + 2):
                # iterate through all coordinates +/- 20 bp of the variant
                # for i in range(var_coord - 20, var_coord + 21):
                i = exon_start - 1
                variant_in_range = True
                wt_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession, var_chr, i)
                if wt_prediction:
                        # and i == exon_start - 1:
                    wt_seq = wt_prediction[0]
                    compare = difflib.SequenceMatcher(None, var_seq, wt_seq)  # compare sequences
                    # same sequence with one base different
                    if compare.ratio() == 89 / 90:
                        canon_prediction = self.refSeqDB.getWildTypeAcceptorPrediction(accession, var_chr,
                                                                                       exon_start - 1)
                        if canon_prediction:
                            canon_prob = canon_prediction[1]
                            deltaCanon = getDeltaCanon(var_prob, canon_prob)
                            # the change in prob must reach threshold and the deltaCanon must be negative for weakening
                            if abs(deltaCanon) >= self.dCthreshold and deltaCanon < 0:
                                self.out_acceptor_count += 1
                                self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                                       str(round(canon_prob, 3)), str(round(deltaCanon, 3)),
                                                       'weakens canonical acceptor')
                        else:
                            self.nowt_count += 1
                            self.nowt_exons.add(accession + '-' + var_chr + ':' + str(exon_start - 1))
                            self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                                   str(round(wt_prediction[1], 3)), 'NA',
                                                   'NA', 'NA', 'NA',
                                                   'no Canonical wild type data found in database')
                else:
                    self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                           'NA', 'NA',
                                           'NA', 'NA', 'NA',
                                           'no wild type data found in database')

        if not variant_in_range:
            self.writeToOutputFile(seqID, str(round(var_prob, 3)),
                                   'NA', 'NA',
                                   'NA', 'NA', 'NA',
                                   'weakening canonical acceptor: variant is not with range of any exon acceptor site')

    def prioritizeAcceptorPredictions(self, acceptor_var_predictions, acceptor_candidates):
        for var_data in acceptor_var_predictions:
            var_chr, var_coord = var_data[0], var_data[1]
            accession, var_seq, var_prob, seqID = var_data[2], var_data[3], var_data[4], var_data[5]
            # can't do anything unless we have the exon bed for this accession
            exons = self.refSeqDB.getGeneExonsCoordinates(accession)

            if exons:
                # CRYPTIC ACTIVATION OF ACCEPTORS #
                if var_prob >= settings.ACCEPTOR_VAR_PROB_CRYPTIC_ACTIVATION_THRESHOLD:  # only proceed with strongish true predictions (more permissive threshold than donors)
                    self.processCrypticActivationAcceptorVariant(exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                                 accession, acceptor_candidates)
                # WEAKENING OF CANONICAL ACCEPTORS #
                if var_prob <= settings.ACCEPTOR_VAR_PROB_WEAKENING_THRESHOLD:  # weakened canonical acceptors can still have a high prob and true classification
                    self.processWeakingOfCanonicalAcceptorVariant(exons, var_chr, var_coord, var_seq, var_prob, seqID,
                                                                  accession)
            else:
                self.nobed_count += 1
                self.unknown_accessions.add(accession)
                self.writeToOutputFile(seqID, str(round(var_prob, 3)), 'NA', 'NA',
                                       'NA', 'NA', 'NA',
                                       'Accession ' + accession + ' not in database')

    def writeToOutputFile(self, seqID, var_prob, wt_prob, delta_var, canon_prob,
                          delta_canon, consequence, info=''):
        out_list = [seqID, var_prob, wt_prob, delta_var,
                    canon_prob, delta_canon, consequence,
                    info]
        self.outputFile.write('\t'.join(out_list) + '\n')

    def prioritizePredictions(self, donorPredictionsFilename, donorCandidatesFilename,
                              acceptorPredictionsFilename, acceptorCandidatesFilename,
                              outputFilename=None):
        self.refSeqDB.connect()

        donor_var_predictions = getPredictions(donorPredictionsFilename)
        donor_candidates = getCandidates(donorCandidatesFilename)

        acceptor_var_predictions = getPredictions(acceptorPredictionsFilename)
        acceptor_candidates = getCandidates(acceptorCandidatesFilename)

        if outputFilename is None:
            timestamp = time.strftime('%Y-%m-%d-%H-%M-%S')
            outputFilename = 'prioritized_predictions_' + timestamp + '_' + str(self.dVthreshold) \
                             + '-' + str(self.dCthreshold) + '.txt'

        self.outputFile = open(outputFilename, 'wt')
        self.outputFile.write('>deltaVar threshold: ' + str(self.dVthreshold) + '\n')
        self.outputFile.write('>deltaCanon threshold: ' + str(self.dCthreshold) + '\n')
        self.outputFile.write(
            '>variant\tP(variant)\tP(ref)\tdeltaVar\tP(canonical)\tdeltaCanon\tconsequence\tinfo\n')  # header

        self.out_count = 0  # number of predictions that are prioritized

        print('### prioritizing donor predictions ###')
        self.prioritizeDonorPredictions(donor_var_predictions, donor_candidates)

        print('### prioritizing acceptor predictions ###')
        self.prioritizeAcceptorPredictions(acceptor_var_predictions, acceptor_candidates)

        self.outputFile.close()
        self.refSeqDB.close()

        print('--------------')
        print('### counts ###')
        print('--------------')
        print('Total Number of Prioritized Predictions: ' + str(self.out_donor_count + self.out_acceptor_count))
        print('Total Number of Donor Prioritized Predictions: ' + str(self.out_donor_count))
        print('Total Number of Acceptor Prioritized Predictions: ' + str(self.out_acceptor_count))
        print('Number of predictions excluded due to missing accession in database: ' + str(self.nobed_count))
        print('Unknown accessions:')
        print(self.unknown_accessions)
        print('Number of predictions excluded due to missing wild type data in database: ' + str(self.nowt_count))
        print('Excluded exons due to missing data in the database for exons:')
        print(self.nowt_exons)

        return outputFilename


def printUsage():
    print("Usage: prioritize_predictions.py donor_candidates_file donor_predictions_file\n\t\t"
          "acceptor_candidates_file acceptor_prediction_file\n\t\t"
          "delta_variant_threshold  delta_canonical_threshold")


def main():
    if len(sys.argv) == 7:
        donorCandidatesFilename = sys.argv[1]
        donorPredictionsFilename = sys.argv[2]
        acceptorCandidatesFilename = sys.argv[3]
        acceptorPredictionsFilename = sys.argv[4]
        dVthreshold, dCthreshold = float(sys.argv[5]), float(sys.argv[6])

        outputFilename = PrioritizePredicitions(dVthreshold, dCthreshold).prioritizePredictions(
            donorPredictionsFilename,
            donorCandidatesFilename,
            acceptorPredictionsFilename,
            acceptorCandidatesFilename)

        print("Prioritized Predictions Written to " + outputFilename)
    else:
        printUsage()


if __name__ == '__main__':
    main()
