# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import collections

import time
from email._header_value_parser import Value

Prediction = collections.namedtuple('Prediction', ['location', 'chromosome', 'coordinate', 'accession', 'sequence',
                                                   'prediction', 'probability', 'sequenceID'])


def formatWindowSizeString(multiscaleComponent):
    if multiscaleComponent.lower() == 'full':
        return 'full'
    else:
        tokens = multiscaleComponent.split('/')
        return '-' + tokens[0] + '+' + tokens[1]


def getPredictions(filename):
    predictions = []

    with open(filename) as predictionsFile:
        for line in predictionsFile:
            if not line.startswith('>'):
                lineTokens = line.split('\t')
                seqID = lineTokens[0]
                # predictionResult = bool(lineTokens[1])
                var_prob = float(lineTokens[2].strip())

                # seq-chr:pos-accession-ref>alt
                seqIDTokens = seqID.split('-')
                seq = seqIDTokens[0]
                location = seqIDTokens[1]
                accession = seqIDTokens[2]

                locationTokens = location.split(':')
                var_chr = locationTokens[0]
                var_coord = int(locationTokens[1])

                value = [var_chr, var_coord, accession, seq, var_prob, seqID]
                predictions.append(value)

    return predictions


def getCandidates(filename):
    candidates = []  # list allows for candidates which can have same chr:coord

    with open(filename) as candidateFile:
        for line in candidateFile:
            # [0] is chr:pos-accession-ref>alt (seqID), [1] is sequence, [2] is variant consequence
            lineTokens = line.split('\t')
            seqID = lineTokens[0]
            seq = lineTokens[1]
            consequence = lineTokens[2].rstrip()
            seq_id_tokens = seqID.split('-')
            location = seq_id_tokens[0]
            accession = seq_id_tokens[1].split('.')[0]

            candidate = [location, seq, consequence, seqID, accession]
            candidates.append(candidate)

    return candidates


# a function to calculate the difference between the probEst of a variant site and the probEst of a WT site
def getDeltaVar(var_prob, wt_prob):
    deltaVar = var_prob - wt_prob
    return deltaVar


# a function to calculate the difference between the probEst of a variant site and the probEst of a nearby canonical site
def getDeltaCanon(var_prob, canon_prob):
    deltaCanon = var_prob - canon_prob
    return deltaCanon


def buildAccessionLocationKey(accession, chromosome, position):
    baseAccession = accession.split('.')[0]
    accessionLocation = baseAccession + '-' + chromosome + ':' + str(position)
    return accessionLocation


def getFileTimeStamp():
    return time.strftime('%Y-%m-%d-%H-%M-%S')

def equalsIgnoreCase(x,y):
    try:
        return x.lower() == y.lower()
    except AttributeError:
        return x == y

def extractWindowSizeValues(window_size):
    try:
        tokens = window_size.split('/')
        return abs(int(tokens[0])), abs(int(tokens[1]))
    except:
        raise ValueError('Invalid window size')

"""
Purpose: To trim sequences based on origin file and site type (i.e. donor, acceptor).
Arguments: Sequence to be trimmed, splice site type, window size of interest
Returns: Trimmed splice sequence.
"""
def trimSeq(sequence, site_type, window_size):
    if equalsIgnoreCase(window_size, 'full'):
        return sequence.strip()
    else:
        left, right = extractWindowSizeValues(window_size)
        seqLen = len(sequence)

        if equalsIgnoreCase(site_type, 'donors'):
            x = max(7 - left, 0)
            y = 9 + right
        elif equalsIgnoreCase(site_type, 'acceptors'):
            x = max(68 - left, 0)
            y = 70 + right
        else:
            raise ValueError('Site type must be donors or acceptors')

        # Fill in trimmed sequence with artificial bases if window extends past sequence length
        z = ''
        if y > seqLen:
            for i in range(seqLen, y):
                z += 'N'
            y = seqLen

        return sequence[x:y] + z


