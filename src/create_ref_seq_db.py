# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sqlite3
import settings
import pickle

def createRefSeqExonsTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Exon (accession TEXT, start INT, end INT)')

    with open('data/refseq_exons_dict.pkl', 'rb') as f:
        data = pickle.load(f)
        print('\tLoaded Ref Seq Exon file...')
        print('\tWriting file content to DB...')
        for accession, exons in data.items():
            for exonCoords in exons:
                rowTuple = (accession, exonCoords[0], exonCoords[1])
                cursor.execute('INSERT INTO Exon VALUES (?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_index ON Exon (accession)')

def createWildTypeDonorPredicitionTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Wild_Type_Donor_Prediction '
                   + '(accession_location TEXT, sequence TEXT, probability REAL, sequence_id TEXT)')

    with open('data/refseq_wt_donor_predicts.pkl', 'rb') as f:
        data = pickle.load(f)
        print('\tLoaded WT Donor Predictions file...')
        print('\tWriting file content to DB...')
        for accession_location, value in data.items():
            rowTuple = (accession_location, value[0], value[1], value[2])
            cursor.execute('INSERT INTO Wild_Type_Donor_Prediction VALUES (?,?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_location_donor_index '
                   + 'ON Wild_Type_Donor_Prediction (accession_location)')

def createWildTypeAcceptorPredicitionTable(cursor):
    cursor.execute('CREATE TABLE IF NOT EXISTS Wild_Type_Acceptor_Prediction '
                   + '(accession_location TEXT, sequence TEXT, probability REAL, sequence_id TEXT)')

    with open('data/refseq_wt_acceptor_predicts.pkl', 'rb') as f:
        data = pickle.load(f)
        print('\tLoaded WT Acceptor Predictions file...')
        print('\tWriting file content to DB...')
        for accession_location, value in data.items():
            rowTuple = (accession_location, value[0], value[1], value[2])
            cursor.execute('INSERT INTO Wild_Type_Acceptor_Prediction VALUES (?,?,?,?)', rowTuple)

    cursor.execute('CREATE INDEX IF NOT EXISTS accession_location_acceptor_index '
                   + 'ON Wild_Type_Acceptor_Prediction (accession_location)')

def main():
    connection = sqlite3.connect(settings.PREDICTION_PRIORITIZATION_DB_FILE_NAME)
    cursor = connection.cursor()

    print('Creating Exon table...')
    createRefSeqExonsTable(cursor)
    connection.commit()
    print('\tFinished')
    # accession = ('NM_021965',)
    # cursor.execute('SELECT start,end FROM Exon WHERE accession = ?', accession)
    # print(cursor.fetchall())
    #
    # print('Creating Wild Type Donor Prediction table...')
    # createWildTypeDonorPredicitionTable(cursor)
    # connection.commit()
    # print('\tFinished')
    #
    # print('Creating Wild Type Acceptor Prediction table...')
    # createWildTypeAcceptorPredicitionTable(cursor)
    # connection.commit()
    # print('\tFinished')

    connection.close()

main()