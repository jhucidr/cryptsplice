# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sqlite3
from collections import namedtuple

import settings
import splice_site_utils


class RefSeqDatabase:
    def __init__(self):
        self.connection = None
        self.cursor = None

    def connect(self):
        self.connection = sqlite3.connect(settings.PREDICTION_PRIORITIZATION_DB_FILE_NAME)
        self.cursor = self.connection.cursor()

    def getGeneExonsCoordinates(self, accession):
        baseAccession = accession.split('.')[0]
        queryTuple = (baseAccession,)
        query = 'SELECT start, end FROM Exon WHERE accession = ? ORDER BY end ASC'

        self.cursor.execute(query, queryTuple)
        result = self.cursor.fetchall()

        return result

    def getWildTypeDonorPrediction(self, accession, chromosome, position):
        return self.getWildTypePrediction(accession, chromosome, position, 'Wild_Type_Donor_Prediction')

    def getWildTypeAcceptorPrediction(self, accession, chromosome, position):
        return self.getWildTypePrediction(accession, chromosome, position, 'Wild_Type_Acceptor_Prediction')

    def getWildTypePrediction(self, accession, chromosome, position, table_name):
        accessionLocation = splice_site_utils.buildAccessionLocationKey(accession, chromosome, position)
        queryTuple = (accessionLocation,)
        query = 'SELECT sequence, probability, sequence_id from {tn} WHERE accession_location = ?'.format(tn=table_name)

        self.cursor.execute(query, queryTuple)
        result = self.cursor.fetchone()
        return result

    def close(self):
        self.connection.close()
