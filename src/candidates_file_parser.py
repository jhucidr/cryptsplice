import re
from collections import namedtuple

SEQ_ID_PATTERN = '([XYxy\d]+):(\d+)-([^\.]+)\.\d+-(.+)>(.+)'
SEQ_PATTERN = '[ATCGUatcgu]+'
TYPE_PATTERN = '(SNP|insertion|deletion)(-non)?-consensus'

Candidate = namedtuple('Candidate', 'seq_id chrom pos accession ref alt seq var_type')
VariantData = namedtuple('VariantData', 'chrom pos accession ref alt')


def get_candidates(filename):
    candidates = []  # list allows for candidates which can have same chr:coord

    with open(filename) as candidateFile:
        line_num = 1
        for line in candidateFile:
            # [0] is chr:pos-accession-ref>alt (seqID), [1] is sequence, [2] is variant consequence
            line_tokens = line.split('\t')

            if len(line_tokens) >= 3:
                seq_id = line_tokens[0].strip()
                seq = line_tokens[1].strip()
                var_type = line_tokens[2].strip()
                variant_data = parse_seq_id(seq_id)

                assert variant_data, filename + ": line " + str(line_num) + " - Invalid sequence ID found " \
                                     + seq_id + " should be in format chromosome:position-accession-ref>alt"
                assert is_sequence_valid(seq), filename + ": line " + str(line_num) + " - Invalid sequence " + seq
                assert is_var_type_valid(var_type), filename + ": line " + str(line_num) \
                                                + " - Invalid variant type " + var_type

                candidates.append(Candidate(seq_id, variant_data.chrom, int(variant_data.pos), variant_data.accession,
                                            variant_data.ref, variant_data.alt, seq, var_type))
            line_num += 1

    return candidates


def parse_seq_id(seq_id):
    match = re.match(SEQ_ID_PATTERN, seq_id)

    if match:
        return VariantData(*match.groups())

    return None


def is_sequence_valid(seq):
    return re.match(SEQ_PATTERN, seq) is not None


def is_var_type_valid(type):
    return re.match(TYPE_PATTERN, type) is not None