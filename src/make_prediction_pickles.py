# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import os
import pickle as pkl

donor_list, acceptor_list = [], []
for filename in os.listdir('.'):
    if filename.startswith('prediction'):
        if 'donor' in filename:
            donor_list.append(filename)
        elif 'acceptor' in filename:
            acceptor_list.append(filename)

print('len(donor_list): ' + str(len(donor_list)))
print('len(acceptor_list): ' + str(len(acceptor_list)))

donor_dict, acceptor_dict = {}, {}
print('### compiling wt donor predictions into dict...')
for filename in donor_list:
    print('adding predictions from ' + filename + '...')
    file = open(filename)
    for line in file:
        if not line.startswith('>'):
            seqID, probEst = line.split('\t')[0], float(line.split('\t')[2].strip())
            acc_loc, seq = '-'.join(seqID.split('-')[1:3]), seqID.split('-')[0]
            assert len(seq) == 15, 'This sequence is not a donor!'
            value = [seq, probEst, seqID]
            donor_dict[acc_loc] = value
print('len(donor_dict.keys()): ' + str(len(donor_dict.keys())))

print('### compiling wt acceptor predictions into dict...')
for filename in acceptor_list:
    print('adding predictions from ' + filename + '...')
    file = open(filename)
    for line in file:
        if not line.startswith('>'):
            seqID, probEst = line.split('\t')[0], float(line.split('\t')[2].strip())
            acc_loc, seq = '-'.join(seqID.split('-')[1:3]), seqID.split('-')[0]
            assert len(seq) == 90, 'This sequence is not an acceptor!'
            value = [seq, probEst, seqID]
            acceptor_dict[acc_loc] = value
print('len(acceptor_dict.keys()): ' + str(len(acceptor_dict.keys())))

print('### dumping dict of donor predictions...')
pkl.dump(donor_dict, open('refseq_wt_donor_predicts.pkl', 'wb'))
print('### dumping dict of acceptor predictions...')
pkl.dump(acceptor_dict, open('refseq_wt_acceptor_predicts.pkl', 'wb'))