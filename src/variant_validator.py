import collections

DONOR_SEQ_LENGTH = 15
DONOR_SPLICE_SITE_START = 7
DONOR_SPLICE_SITE_END = 9
DONOR_SPLICE_SITE = "GT"
DONOR_SEQUENCE_LENGTH_ERROR = 'Sequence must be ' + str(DONOR_SEQ_LENGTH) + ' bases long'
DONOR_SPLICE_SITE_POSITION_ERROR = 'Splice site must be in the (' + str(DONOR_SPLICE_SITE_START) + ',' \
                                      + str(DONOR_SPLICE_SITE_END) + ') position'

ACCEPTOR_SEQ_LENGTH = 90
ACCEPTOR_SPLICE_SITE_START = 68
ACCEPTOR_SPLICE_SITE_END = 70
ACCEPTOR_SPLICE_SITE = "AG"
ACCEPTOR_SEQUENCE_LENGTH_ERROR = 'Sequence must be ' + str(ACCEPTOR_SEQ_LENGTH) + ' bases long'

SPLICE_SITE_POSITION_ERROR = 'Loss of canonical splice site known to cause missplicing'
VARIANT_OUT_OF_RANGE_ERROR = 'Variant position is not within range of splice site'
VARIANT_ALTERS_SPLICE_SITE_ERROR = 'Variant alters canonical splice site'
MISSING_ACCESSION_ERROR = 'Accession not found in database'

InvalidCandidate = collections.namedtuple('InvalidCandidate', 'seq_id error')


def is_donor_splice_site_in_correct_position(sequence):
    return sequence[DONOR_SPLICE_SITE_START:DONOR_SPLICE_SITE_END] == DONOR_SPLICE_SITE


def is_acceptor_splice_site_in_correct_position(sequence):
    return sequence[ACCEPTOR_SPLICE_SITE_START:ACCEPTOR_SPLICE_SITE_END] == ACCEPTOR_SPLICE_SITE


def get_exon_in_range_of_donor_site(var_pos, exons):
    for exon in exons:
        exon_end = exon[1]
        # -3,5 range around splice site located at +1,+2
        if var_pos in range(exon_end - 3, exon_end + 6):
            return exon

    return None


def get_exon_in_range_of_acceptor_site(var_pos, exons):
    for exon in exons:
        exon_start = exon[0]
        # -22,+1 range around splice site located at 0,-1
        if var_pos in range(exon_start - 23, exon_start + 2):
            return exon

    return None


def is_variant_outside_donor_site(var_pos, exon):
    exon_end = exon[1]
    return var_pos != exon_end + 1 and var_pos != exon_end + 2


def is_variant_outside_acceptor_site(var_pos, exon):
    exon_start = exon[0]
    return var_pos != exon_start and var_pos != exon_start - 1


class VariantValidator:
    def __init__(self, candidates, ref_seq_db):
        self.candidates = candidates
        self.ref_seq_db = ref_seq_db
        self.valid_candidates = []
        self.invalid_candidates = []

    def validate_donor_variant(self, candidate):
        exons = self.ref_seq_db.getGeneExonsCoordinates(candidate.accession)

        if exons:
            nearest_exon = get_exon_in_range_of_donor_site(candidate.pos, exons)

            if nearest_exon:
                if is_variant_outside_donor_site(candidate.pos, nearest_exon):
                    self.valid_candidates.append(candidate)
                else:
                    self.invalid_candidates.append(InvalidCandidate(candidate.seq_id, VARIANT_ALTERS_SPLICE_SITE_ERROR))
            else:
                self.valid_candidates.append(candidate)
        else:
            self.invalid_candidates.append(InvalidCandidate(candidate.seq_id, MISSING_ACCESSION_ERROR))

    def validate_donor_candidates(self):
        for candidate in self.candidates:
            seq = candidate.seq
            seq_id = candidate.seq_id

            if len(seq) == DONOR_SEQ_LENGTH:
                if is_donor_splice_site_in_correct_position(seq):
                    self.validate_donor_variant(candidate)
                else:
                    self.invalid_candidates.append(InvalidCandidate(seq_id, SPLICE_SITE_POSITION_ERROR))
            else:
                self.invalid_candidates.append(InvalidCandidate(seq_id, DONOR_SEQUENCE_LENGTH_ERROR))

        return self.valid_candidates

    def validate_acceptor_variant(self, candidate):
        exons = self.ref_seq_db.getGeneExonsCoordinates(candidate.accession)

        if exons:
            nearest_exon = get_exon_in_range_of_acceptor_site(candidate.pos, exons)

            if nearest_exon:
                if is_variant_outside_acceptor_site(candidate.pos, nearest_exon):
                    self.valid_candidates.append(candidate)
                else:
                    self.invalid_candidates.append(InvalidCandidate(candidate.seq_id, VARIANT_ALTERS_SPLICE_SITE_ERROR))
            else:
                self.valid_candidates.append(candidate)
        else:
            self.invalid_candidates.append(InvalidCandidate(candidate.seq_id, MISSING_ACCESSION_ERROR))

    def validate_acceptor_candidates(self):
        for candidate in self.candidates:
            seq = candidate.seq
            seq_id = candidate.seq_id

            if len(seq) == ACCEPTOR_SEQ_LENGTH:
                if is_acceptor_splice_site_in_correct_position(seq):
                    self.validate_acceptor_variant(candidate)
                else:
                    self.invalid_candidates.append(InvalidCandidate(seq_id, SPLICE_SITE_POSITION_ERROR))
            else:
                self.invalid_candidates.append(InvalidCandidate(seq_id, ACCEPTOR_SEQUENCE_LENGTH_ERROR))

        return self.valid_candidates

    def pretty_print_invalid_candidates(self):
        for invalid_candidate in self.invalid_candidates:
            print(invalid_candidate.seq_id + " - " + invalid_candidate.error)

# def no_donor_site_loss_check(candidate, gene_info):
#     gene_start = gene_info[1]
#     seq = gene_info[3]
#     var_index = candidate.pos - gene_start
#     var_type = candidate.var_type
#     alt = candidate.alt
#
#     if 'SNP' in var_type:
#         return site_mutation_check.checkDonorSNP(seq, var_index, alt)
#     elif 'insertion' in var_type:
#         return site_mutation_check.checkDonorINSorDUP(seq, var_index, alt)
#     elif 'deletion' in var_type:
#         return site_mutation_check.checkDonorDel(seq, var_index, alt)
#     else:
#         return True
#
#
# def is_valid_donor_candidate(candidate, gene_info, exons):
#     range_check = True
#     site_mut_check = True
#
#     if settings.EXCLUDE_VARIANTS_OUTSIDE_SPLICE_SITE_RANGE and exons:
#         range_check = variant_in_range_check.is_variant_in_range_of_donor_site(candidate.pos, exons)
#     if settings.EXCLUDE_VARIANTS_WITH_SPLICE_SITE_LOSS:
#         site_mut_check = no_donor_site_loss_check(candidate, gene_info)
#
#     print_skipped_candidate_message(range_check, site_mut_check, candidate.seq_id)
#
#     return range_check and site_mut_check
#
#
# def get_donor_candidates_with_no_splice_site_mutation(candidates, ref_seq_dict, ref_seq_db):
#     filtered_candidates = []
#
#     for candidate in candidates:
#         accession = candidate.accession
#
#         if accession in ref_seq_dict:
#             gene_info = ref_seq_dict[accession]
#             exons = ref_seq_db.getGeneExonsCoordinates(accession)
#
#             if is_valid_donor_candidate(candidate, gene_info, exons):
#                 filtered_candidates.append(candidate)
#         else:
#             print('Site Mutation Check Unknown Accession: ' + accession)
#             filtered_candidates.append(candidate)
#
#     return filtered_candidates
#
#
# def no_acceptor_site_loss_check(candidate, gene_info):
#     gene_start = gene_info[1]
#     seq = gene_info[3]
#     var_index = candidate.pos - gene_start
#     var_type = candidate.var_type
#     alt = candidate.alt
#
#     if 'SNP' in var_type:
#         return site_mutation_check.checkAcceptorSNP(seq, var_index, alt)
#     elif 'insertion' in var_type:
#         return site_mutation_check.checkAcceptorINSorDUP(seq, var_index, alt)
#     elif 'deletion' in var_type:
#         return site_mutation_check.checkAcceptorDel(seq, var_index, alt)
#     else:
#         return True
#
#
# def is_valid_acceptor_candidate(candidate, gene_info, exons):
#     range_check = True
#     site_mut_check = True
#     exon = None
#
#     if exons:
#         exon = get_exon_near_by_candidate_acceptor_site(candidate, exons)
#
#     if settings.EXCLUDE_VARIANTS_OUTSIDE_SPLICE_SITE_RANGE and exon:
#         range_check = variant_in_range_check.is_variant_in_range_of_acceptor_site(candidate.pos, exons)
#     if settings.EXCLUDE_VARIANTS_WITH_SPLICE_SITE_LOSS:
#         site_mut_check = no_acceptor_site_loss_check(candidate, gene_info)
#
#     print_skipped_candidate_message(range_check, site_mut_check, candidate.seq_id)
#
#     return range_check and site_mut_check
#
#
# def print_skipped_candidate_message(range_check, site_mut_check, seq_id):
#     if not range_check:
#         print('Excluding variant - variant is not within range of splice site: ' + seq_id)
#     elif not site_mut_check:
#         print('Excluding variant - variant causes loss of splice site: ' + seq_id)
#
#
# def get_exon_near_by_candidate_acceptor_site(candidate, exons):
#     for exon in exons:
#         exon_start = exon[0]
#         if candidate.pos in range(exon_start - 22, exon_start + 2):
#             return exon
#
#     return None
#
#
# def get_acceptor_candidates_with_no_splice_site_mutation(candidates, ref_seq_dict, ref_seq_db):
#     filtered_candidates = []
#
#     for candidate in candidates:
#         accession = candidate.accession
#
#         if accession in ref_seq_dict:
#             gene_info = ref_seq_dict[accession]
#             exons = ref_seq_db.getGeneExonsCoordinates(accession)
#
#             if exons:
#                 exon = get_exon_near_by_candidate_acceptor_site(candidate, exons)
#
#                 if exon:
#                     if is_valid_acceptor_candidate(candidate, gene_info, exons):
#                         filtered_candidates.append(candidate)
#             else:
#                 print_skipped_candidate_message(False, False, candidate.seq_id)
#         else:
#             print('Site Mutation Check Unknown Accession: ' + accession)
#             filtered_candidates.append(candidate)
#
#     return filtered_candidates
#
#
# def filter_candidates(donor_candidates_filename, acceptor_candidates_filename):
#     donor_candidates = candidates_file_parser.get_candidates(donor_candidates_filename)
#     acceptor_candidates = candidates_file_parser.get_candidates(acceptor_candidates_filename)
#
#     if settings.EXCLUDE_VARIANTS_WITH_SPLICE_SITE_LOSS or settings.EXCLUDE_VARIANTS_OUTSIDE_SPLICE_SITE_RANGE:
#         ref_seq_dict = pickle.load(open(settings.REF_SEQ_GENES_FILE_NAME, 'rb'))
#         ref_seq_db = RefSeqDatabase()
#         ref_seq_db.connect()
#         print("Validating Donor Candidates")
#         filtered_donor_candidates = get_donor_candidates_with_no_splice_site_mutation(donor_candidates, ref_seq_dict,
#                                                                                       ref_seq_db)
#         print("Validating Acceptor Candidates")
#         filtered_acceptor_candidates = get_acceptor_candidates_with_no_splice_site_mutation(acceptor_candidates,
#                                                                                             ref_seq_dict, ref_seq_db)
#         ref_seq_db.close()
#         return filtered_donor_candidates, filtered_acceptor_candidates
#     else:
#         return donor_candidates, acceptor_candidates
