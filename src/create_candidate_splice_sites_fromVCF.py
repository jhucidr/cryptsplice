# Copyright 2017
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
#  to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import collections
import os
import re
import sys
import settings

try:
    import cPickle as pickle
except:
    import pickle

def isValidAcceptorCandidiate(candidate_mut_seq):
    if settings.VALIDATE_USER_SUPPLIED_VARIANTS:
        return candidate_mut_seq[68:70] == 'AG'
    else:
        return True

def isValidDonorCandidate(candidate_mut_seq):
    if settings.VALIDATE_USER_SUPPLIED_VARIANTS:
        return candidate_mut_seq[7:9] == 'GT'
    else:
        return True

class CandidateSpliceSiteExtractor:
    var_dict = collections.OrderedDict()
    refseq_dict = {}
    unique_donor_vars = set() # track number of variants that actually create sequences
    unique_acceptor_vars = set()

    def __init__(self, vcf_file_name):
        self.vcf_file_name = vcf_file_name
        self.populateVariantDict()
        self.refseq_dict = pickle.load(open(settings.REF_SEQ_GENES_FILE_NAME, 'rb'))

    def populateVariantDict(self):
        noacc = set() # track number of variants left out
        vcf_file = open(self.vcf_file_name)
        for line in vcf_file:
            if not line.startswith('#'):
                lineValues = line.split('\t')
                match = re.search(r'\|Transcript\|(NM_[0-9]+\.[0-9]+)\|protein_coding\|', lineValues[7])
                var_chr, var_coord, ref, alt_str = lineValues[0], int(lineValues[1]), lineValues[3], lineValues[4]
                alt_list = alt_str.split(",")

                for alt in alt_list:
                    try:
                        accession = match.group(1)
                        if len(ref) == len(alt):
                            varType = 'snp'
                        elif len(ref) < len(alt):
                            varType = 'ins'
                        elif len(ref) > len(alt):
                            varType = 'del'
                        key = var_chr + ':' + str(var_coord) + '-' + accession + '-' + ref + '>' + alt
                        # key = var_chr + ':' + str(var_coord) + '-' + ref + '>' + alt + '-' + accession
                        self.var_dict[key] = [accession, var_coord, ref, alt, varType]
                    except AttributeError:
                        noacc.add(var_chr + ':' + str(var_coord) + '-' + ref + '>' + alt)
        print('# Skipped Variants: ' + str(len(noacc)))

    def generateCandidateSpliceSites(self):
        vcfName = os.path.basename(self.vcf_file_name)
        out_donor_file = open('candidate_' + vcfName + '_vcf_donor_sequences.txt', 'wt')
        out_acceptor_file = open('candidate_' + vcfName + '_vcf_acceptor_sequences.txt', 'wt')
        unique_donor_vars = set() # track number of variants that actually create sequences
        unique_acceptor_vars = set()

        # make sequences from VCF variant dict
        print('Making candidate splice sequences from ' + vcfName + ' variants...')
        for variant, varData in self.var_dict.items():
            accession, var_coord, ref, alt, varType = varData[0], varData[1], varData[2], varData[3], varData[4]
            accession = accession.split('.')[0] # remove accession version number

            # use gene accession number to get coords and sequence from refseq_dict
            try:
                # will break if accession number isn't in refseq fasta dict
                ref_seq_gene = self.refseq_dict[accession]
                gene_chr, gene_start, gene_stop, sequence = ref_seq_gene[0], ref_seq_gene[1], ref_seq_gene[2], ref_seq_gene[3]
                donor_indices, acceptor_indices = self.getDonorAndAcceptorDinucIndices(sequence)
                # get dinucleotide indices from wild-type fasta sequence
                # donor_indices = [m.start(0) for m in re.finditer(r'GT', sequence)]
                # acceptor_indices = [m.start(0) for m in re.finditer(r'AG', sequence)]
                tupleSeq = tuple(sequence) # IMMUTABLE!

                # get indices
                var_index = var_coord - gene_start
                end_index = gene_stop - gene_start # last index in gene
                assert (end_index+1) == len(tupleSeq), 'End index and fasta sequence length do not match!'

                # variant cannot be at first or last nucleotide in the gene or weird CGA CNV identifier
                if 0 < var_index < end_index:
                    # print('Checking eligibility and making splice sequence from ' + gene_chr + ':' + str(var_coord) + '...')

                    if varType == 'snp':

                        # check first if SNP creates consensus dinucleotide, get mutant candidate sequence
                        donor_seqs, acceptor_seqs = self.checkConsensusSNP(tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tSNP-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tSNP-consensus\n')
                            unique_acceptor_vars.add(variant)

                        # then check if variant occurs near existing dinucleotides and make mutant candidate sequence
                        donor_seqs, acceptor_seqs = self.makeNonConsensusSNP(donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tSNP-non-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tSNP-non-consensus\n')
                            unique_acceptor_vars.add(variant)

                    # create candidate sequences from insertions
                    elif varType == 'ins':

                        n = len(alt) # length of inserted sequence
                        var_index = var_index + n # reposition to first variant base

                        # check first to see if insertion creates a consensus dinucleotide, get mutant candidate sequence
                        donor_seqs, acceptor_seqs = self.checkConsensusINSorDUP(tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tinsertion-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tinsertion-consensus\n')
                            unique_acceptor_vars.add(variant)

                        # then check if insertion occurs near existing dinucleotides and make mutant candidate sequences
                        donor_seqs, acceptor_seqs = self.makeNonConsensusINSorDUP(donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tinsertion-non-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tinsertion-non-consensus\n')
                            unique_acceptor_vars.add(variant)

                    # create candidate sequences from deletions
                    elif varType == 'del':

                        # n = len(alt) # length of deleted sequence

                        # check first to see if insertion creates a consensus dinucleotide, get mutant candidate sequence
                        donor_seqs, acceptor_seqs = self.checkConsensusDel(tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tdeletion-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tdeletion-consensus\n')
                            unique_acceptor_vars.add(variant)

                        # then check if deletion occurs near existing dinucleotides and make mutant candidate sequences
                        donor_seqs, acceptor_seqs = self.makeNonConsensusDEL(donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt)
                        for seq in donor_seqs:
                            out_donor_file.write(variant + '\t' + seq + '\tdeletion-non-consensus\n')
                            unique_donor_vars.add(variant)
                        for seq in acceptor_seqs:
                            out_acceptor_file.write(variant + '\t' + seq + '\tdeletion-non-consensus\n')
                            unique_acceptor_vars.add(variant)
            except KeyError:
                pass

        print('len(unique_donor_vars): ' + str(len(unique_donor_vars)))
        print('len(unique_acceptor_vars): ' + str(len(unique_acceptor_vars)))
        print('len(union_set): ' + str(len(unique_donor_vars.union(unique_acceptor_vars))))

        out_donor_file.close()
        out_acceptor_file.close()

        if len(unique_acceptor_vars) == 0 and len(unique_donor_vars) == 0:
           raise ValueError('No Candidates Found in VCF, make sure you are using an annotated VCF')

        return out_donor_file.name, out_acceptor_file.name

    # def regExItUp(self, sequence):
    #     return [m.start(0) for m in re.finditer(r'GT', sequence)], [m.start(0) for m in re.finditer(r'AG', sequence)]

    def getDonorAndAcceptorDinucIndices(self, sequence):
        donor_indices = []
        acceptor_indices = []

        for i in range(0, len(sequence)):
            try:
                value = sequence[i]

                if value == "G":
                    if sequence[i+1] == "T":
                        donor_indices.append(i)
                elif value == "A":
                    if sequence[i+1] == "G":
                        acceptor_indices.append(i)
            except IndexError:
                break

        return donor_indices, acceptor_indices

    def checkConsensusSNP(self, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], [] # lists to hold final sequences
        try: # won't index if var_index too close to gene end
            # tuple must be made into a list for altering
            tempSeq = list(tupleSeq)
            tempSeq[var_index] = alt # change variant
            mutSeq_full = ''.join(tempSeq)

            # there are four possible scenarios that could create dinucleotides; multiple can occur

            # variant is G in GT; alt allele is first
            if (alt + mutSeq_full[var_index + 1])  == 'GT':
                # donor sequences are -7, +6
                x = var_index - 7
                y = var_index + 8
                # check if sequence will overlap exon and if not create mutant sequence
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidDonorCandidate(mutSeq_trim):
                        donor_seqs.append(mutSeq_trim)

            # variant is T in GT; alt allele is second
            if (mutSeq_full[var_index - 1] + alt)  == 'GT':
                # donor sequences are -7, +6
                x = var_index - 8
                y = var_index + 7
                # check if sequence will overlap exon and if not create mutant sequence
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidDonorCandidate(mutSeq_trim):
                        donor_seqs.append(mutSeq_trim)

            # variant is A in AG; alt allele is first
            if (alt + mutSeq_full[var_index + 1])  == 'AG':
                # acceptor sequences are -68, +20
                x = var_index - 68
                y = var_index + 22
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidAcceptorCandidiate(mutSeq_trim):
                        acceptor_seqs.append(mutSeq_trim)

            # variant is G in AG; alt allele is second
            if (mutSeq_full[var_index - 1] + alt)  == 'AG':
                # acceptor sequences are -68, +20
                x = var_index - 69
                y = var_index + 21
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidAcceptorCandidiate(mutSeq_trim):
                        acceptor_seqs.append(mutSeq_trim)

        except IndexError:
            print('The candidate variant overlaps the exon!')

        return donor_seqs, acceptor_seqs

    def checkConsensusDel(self, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], []
        try:
            n = len(alt) # length of deleted sequence

            # tuple must be made into a list for deletion
            tempSeq = list(tupleSeq)
            tempSeq[var_index+1:var_index+n+1] = '' # delete variant
            mutSeq_full = ''.join(tempSeq)

            # set variables for centering sequence based on where dinucleotide is created
            if mutSeq_full[var_index:var_index+2] == 'GT':
                x = var_index - 7
                y = var_index + 8
                # check if sequence will overlap exon and if not create mutant sequence
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidDonorCandidate(mutSeq_trim):
                        donor_seqs.append(mutSeq_trim)
            if mutSeq_full[var_index:var_index+2] == 'AG':
                x = var_index - 68
                y = var_index + 22
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidAcceptorCandidiate(mutSeq_trim):
                        acceptor_seqs.append(mutSeq_trim)

        except IndexError:
            print('The candidate variant overlaps the exon!')

        return donor_seqs, acceptor_seqs

    def checkConsensusINSorDUP(self, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], []
        try: # won't index if var_index too close to gene end
            n = len(alt) # length of inserted sequence

            # tuple must be made into a list for insertion
            tempSeq = list(tupleSeq)
            tempSeq.insert(var_index, alt) # insert variant
            mutSeq_full = ''.join(tempSeq)

            # there are four possible scenarios from an insertion; multiple can occur
            if (mutSeq_full[var_index-1] + alt[0]) == 'GT':
                x = var_index - 8
                y = var_index + 7
                # check if sequence will overlap exon and if not create mutant sequence
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidDonorCandidate(mutSeq_trim):
                        donor_seqs.append(mutSeq_trim)
            if (alt[-1] + mutSeq_full[var_index+n]) == 'GT':
                x = var_index + n - 8
                y = var_index + n + 7
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidDonorCandidate(mutSeq_trim):
                        donor_seqs.append(mutSeq_trim)

            if (mutSeq_full[var_index-1] + alt[0]) == 'AG':
                x = var_index - 69
                y = var_index + 21
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidAcceptorCandidiate(mutSeq_trim):
                        acceptor_seqs.append(mutSeq_trim)
            if (alt[-1] + mutSeq_full[var_index+n]) == 'AG':
                x = var_index + n - 69
                y = var_index + n + 21
                if x > 0 and y < end_index:
                    mutSeq_trim = mutSeq_full[x:y]
                    if isValidAcceptorCandidiate(mutSeq_trim):
                        acceptor_seqs.append(mutSeq_trim)

        except IndexError:
            print('The candidate variant overlaps the exon!')

        return donor_seqs, acceptor_seqs

    def makeNonConsensusSNP(self, donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], [] # lists to hold final sequences
        tempSeq = list(tupleSeq) # mutate a temporary list, not original full gene sequence

        tempSeq[var_index] = alt
        mutSeq_full = ''.join(tempSeq)
        try:
            assert tupleSeq[var_index] != mutSeq_full[var_index], 'Alt allele not substituted!'
        except AssertionError:
            print('Alt allele not substituted!')
            pass

        ### GET WINDOWS INVOLVING VARIANT CENTERED AROUND EXISTING DONORS AND ACCEPTORS ###

        # get indices of existing donors with the variant within -3, +5 (smallest window), leave out dints involving variant
        donor_indices = [x for x in donor_indices if var_index in range(x-3, x+7) and (var_index != x and var_index != x+1)]
        # extract -7, +6 sequence around each existing dinucleotide
        for i in donor_indices:
            x = i - 7
            y = i + 8
            # check if sequence will overlap exon and if not trim mutant sequence
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidDonorCandidate(mutSeq_trim):
                    donor_seqs.append(mutSeq_trim)

        # get indices of existing acceptors with the variant within -22, +1 (smallest window), leave out dints involving variant
        acceptor_indices = [x for x in acceptor_indices if var_index in range(x-22, x+3) and (var_index != x and var_index != x+1)]
        # extract -68, +20 sequence around each existing dinucleotide
        for i in acceptor_indices:
            x = i - 68
            y = i + 22
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidAcceptorCandidiate(mutSeq_trim):
                    acceptor_seqs.append(mutSeq_trim)

        return donor_seqs, acceptor_seqs

    def makeNonConsensusINSorDUP(self, donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], [] # lists to hold final sequences
        tempSeq = list(tupleSeq) # mutate a temporary list, not original full gene sequence
        n = len(alt) # length of inserted sequence
        tempSeq.insert(var_index, alt) # insert variant
        mutSeq_full = ''.join(tempSeq)

        ### GET WINDOWS INVOLVING VARIANT CENTERED AROUND EXISTING DONORS AND ACCEPTORS ###

        # get indices of existing donors with any part of the insertion within -3, +5, leave out dints split by variant
        donor_indices = [x for x in donor_indices if var_index in range((x+n)-3-(n-1), x+7) and var_index != x+1]
        # extract -7, +6 sequence around the dinucleotide
        for i in donor_indices:
            if var_index <= i: # insertion is 5' of dint
                i = i + n # reset index of dint for insertion
                x = i - 7
                y = i + 8
            else: # insertion is 3' of dint, won't change dint index
                x = i - 7
                y = i + 8
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidDonorCandidate(mutSeq_trim):
                    donor_seqs.append(mutSeq_trim)

        # get indices of existing acceptors with any part of the insertion within -22, +1, leave out dints split by variant
        acceptor_indices = [x for x in acceptor_indices if var_index in range((x+n)-22-(n-1), x+3) and var_index != x+1]
        # extract -68, +20 sequence around the dinucleotide
        for i in acceptor_indices:
            if var_index <= i: # insertion is 5' of dint
                i = i + n # reset index of dint for insertion
                x = i - 68
                y = i + 22
            else: # insertion is 3' of dint, won't change dint index
                x = i - 68
                y = i + 22
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidAcceptorCandidiate(mutSeq_trim):
                    acceptor_seqs.append(mutSeq_trim)

        return donor_seqs, acceptor_seqs

    def makeNonConsensusDEL(self, donor_indices, acceptor_indices, tupleSeq, var_index, end_index, alt):
        donor_seqs, acceptor_seqs = [], [] # lists to hold final sequences
        tempSeq = list(tupleSeq) # mutate a temporary list, not original full gene sequence
        n = len(alt) # length of inserted sequence
        tempSeq[var_index:var_index+n] = '' # delete variant
        mutSeq_full = ''.join(tempSeq)

        ### GET WINDOWS INVOLVING VARIANT CENTERED AROUND EXISTING DONORS AND ACCEPTORS ###

        # get indices of existing donors with the deletion within -3, +5, leave out dints involving the deletion
        donor_indices = [x for x in donor_indices if var_index in range(x-n-3, x+7) and x not in range(var_index-1, var_index+n+1)]
        # extract -7, +6 sequence around the dinucleotide
        for i in donor_indices:
            if var_index <= i: # deletion is 5' of dint
                i = i - n # reset index of dint for deletion
                x = i - 7
                y = i + 8
            else: # deletion is 3' of dint, won't change dint index
                x = i - 7
                y = i + 8
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidDonorCandidate(mutSeq_trim):
                    donor_seqs.append(mutSeq_trim)

        # get indices of existing acceptors with any part of the insertion within -22, +1, leave out dints split by variant
        acceptor_indices = [x for x in acceptor_indices if var_index in range(x-n-22, x+3) and x not in range(var_index-1, var_index+n+1)]
        # extract -68, +20 sequence around the dinucleotide
        for i in acceptor_indices:
            if var_index <= i: # insertion is 5' of dint
                i = i - n # reset index of dint for deletion
                x = i - 68
                y = i + 22
            else: # deletion is 3' of dint, won't change dint index
                x = i - 68
                y = i + 22
            if x > 0 and y < end_index:
                mutSeq_trim = mutSeq_full[x:y]
                if isValidAcceptorCandidiate(mutSeq_trim):
                    acceptor_seqs.append(mutSeq_trim)

        return donor_seqs, acceptor_seqs

def main():
    vcf = sys.argv[1]
    candidate_splice_site_extractor = CandidateSpliceSiteExtractor(vcf)

    candidate_splice_site_extractor.generateCandidateSpliceSites()

if __name__ == '__main__':
    main()